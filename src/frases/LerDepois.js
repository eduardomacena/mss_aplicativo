import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  ImageBackground,
  Picker,
  Platform,
  BackHandler,
  Modal,
} from 'react-native';
import Slider from 'react-native-simple-slider';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'
import { vencido } from '../utils/consultaPlano'

class LerDepois extends Component {
  state = {
    num: 0,
    value1: 0,
    value2: 0,
    tempo: 0,
    TQR: 0,
    items: [
      { frase: 'Responda pela escala RPE 30 min. após o treino: Como foi o treino?' },
      { frase: 'Em uma escala de 0 a 100% Quanto do treino conseguiu completar?' },
      { frase: 'Quanto tempo durou o seu treino?' }],
    modalVisibleSenha: false,
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('ex')
      return true;
    })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';
    var pathL = RNFS.DocumentDirectoryPath + '/lerantes.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].idUsuario),
            idDesk: String(JSON.parse(contents)[0].idDesk),
            tecnico: String(JSON.parse(contents)[0].tecnico)
          })

          if (vencido(String(JSON.parse(contents)[0].idUsuario))) {
            alert('Seu plano expirou, renove seu plano!')
            this.props.navigation.navigate('Financeiro')
            return false
          }

          this.buscaPlano()
        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );

          this.props.navigation.navigate('ex')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });

    if (this.state.idUsuario) {
      RNFS.readFile(pathL, 'utf8')
        .then((contents) => {
          if (!(contents == '[{}]')) {
            this.setState({
              TQR: String(JSON.parse(contents)[0].TQR)
            })
          } else {
            alert('Você deve responder as perguntas "Responda antes do treino"')
            this.props.navigation.navigate('ex')
          }
        })
        .catch((err) => {
          this.setState({ idUsuario: '0' })
          alert(err.message)
          console.log(err.message, err.code);
        });
    }

  }

  avancaFrase = () => {
    this.setState({
      num: this.state.num == (this.state.items.length - 1) ? this.state.num : (this.state.num + 1)
    })
  }

  retroFrase = () => {
    this.setState({
      num: this.state.num == 0 ? 0 : (this.state.num - 1)
    })
  }

  dataAtualFormatada = () => {
    var data = new Date(),
      dia = data.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data.getFullYear();
    return diaF + "/" + mesF + "/" + anoF;
  }

  enviaQuestionario = async () => {
    this.setState({
      refresh: false
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      let dados = {
        "Data": String(this.dataAtualFormatada()),
        "TQR": String(this.state.TQR),
        "RPE": String(this.state.value1),
        "Tempo": String(this.state.tempo),
        "Treino": String(this.state.value2)
      }

      fetch(urlAPI + '/pse/post.php?tecnico=' + this.state.tecnico + '&atleta=' + this.state.idDesk, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(dados)
      }
      ).then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.message == "pse criado") {
            alert("Enviado com sucesso")
            this.props.navigation.navigate('ex')
          } else {
            alert("falhou, tente mais tarde")
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + JSON.stringify(error));
          this.setState({
            refresh: false
          })
        });
    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  buscaPlano = async () => {
    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(urlAPI + '/planos/getativos.php?atleta=' + this.state.idUsuario, {}
      ).then((response) => response.json())
        .then((responseJson) => {
          if (!responseJson.message) {

          } else {
            alert('Plano expirado, ative seu plano.')
            this.props.navigation.navigate('Financeiro')
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });
    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  /* tela principal */
  render() {
    return (
      <View style={{ backgroundColor: '#1b263a', flex: 1 }}>
        <StatusBar
          barStyle='light-content'
          backgroundColor='#1b263a' />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          elevation: 3,
          shadowColor: '#000',
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.5,
          shadowRadius: 2,
          flex: 1,
          marginTop: Platform.OS == "android" ? 20 : 40,
          backgroundColor: '#1b263a'
        }}>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
          </TouchableOpacity>

          <Text style={{
            fontWeight: 'bold',
            fontSize: 16,
            marginTop: 12,
            color: 'white',
            marginLeft: -15
          }}>DEPOIS DO TREINO</Text>

          <View style={{ width: '33.33%' }} />
        </View>

        <View style={{ flex: 1, justifyContent: 'space-between', backgroundColor: 'white' }}>
          <View />
          <View
            style={{
              flexDirection: 'row',
              marginBottom: 20,
              alignItems: 'center',
              justifyContent: 'space-between',
              marginHorizontal: 20
            }}>
            <TouchableOpacity
              onPress={() => { this.retroFrase() }}
              style={{
                height: 50,
                width: 100,
                justifyContent: 'center',
                borderRadius: 40,
                backgroundColor: '#1b263a'
              }}>
              <Text style={{
                color: 'white',
                textAlign: 'center',
                fontSize: 11
              }}>VOLTAR</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {

                if (this.state.num == 2) {
                  this.enviaQuestionario()
                } else {
                  this.avancaFrase()
                }

              }}
              style={{
                height: 50,
                width: 100,
                justifyContent: 'center',
                borderRadius: 40,
                backgroundColor: '#2ecc71'
              }}>
              <Text style={{
                color: 'white',
                textAlign: 'center',
                fontSize: 11
              }}>{this.state.num == 2 ? 'ENVIAR' : 'PRÓXIMO'}</Text>
            </TouchableOpacity>
          </View>

        </View>

        <View style={{
          position: 'absolute',
          height: '60%',
          width: '80%',
          alignSelf: 'center',
          justifyContent: 'center',
          borderRadius: 20,
          marginTop: '30%',
          alignItems: 'center',
          backgroundColor: 'white',
          elevation: 3,
          shadowColor: '#000',
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.5,
          shadowRadius: 2,
        }}>
          <Text style={{
            fontSize: 20,
            fontWeight: 'bold',
            textAlign: 'center',
            marginHorizontal: 40
          }}>
            {this.state.items[this.state.num].frase}
          </Text>


          {this.state.num == 0 ?
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                marginVertical: 40,
                color: this.state.value1 == 0 ? '#54a0ff' :
                  this.state.value1 == 1 ? '#2e86de' :
                    this.state.value1 == 2 ? '#7bed9f' :
                      this.state.value1 == 3 ? '#2ed573' :
                        this.state.value1 == 4 ? '#f6e58d' :
                          this.state.value1 == 5 ? '#ffbe76' :
                            this.state.value1 == 6 ? '#ffbe76' :
                              this.state.value1 == 7 ? '#ff9f43' :
                                this.state.value1 == 8 ? '#ff9f43' :
                                  this.state.value1 == 9 ? '#ff6b6b' :
                                    this.state.value1 == 10 ? '#e74c3c' : null
                ,
                marginHorizontal: 40
              }}>
                {this.state.value1 == 0 ? 'Muito, Muito Leve' :
                  this.state.value1 == 1 ? 'Muito Leve' :
                    this.state.value1 == 2 ? 'Leve' :
                      this.state.value1 == 3 ? 'Moderado' :
                        this.state.value1 == 4 ? 'Pouco Intenso' :
                          this.state.value1 == 5 ? 'Intenso' :
                            this.state.value1 == 6 ? 'Intenso' :
                              this.state.value1 == 7 ? 'Muito Intenso' :
                                this.state.value1 == 8 ? 'Muito Intenso' :
                                  this.state.value1 == 9 ? 'Muito, Muito Intenso' :
                                    this.state.value1 == 10 ? 'Exaustivo' : null
                }
              </Text>

              <Slider
                value={this.state.value1}
                minimumValue={0}
                maximumValue={10}
                sliderHeight={20}
                thumbTintColor={'#1b263a'}
                minimumTrackTintColor={this.state.value1 == 0 ? '#54a0ff' :
                  this.state.value1 == 1 ? '#2e86de' :
                    this.state.value1 == 2 ? '#7bed9f' :
                      this.state.value1 == 3 ? '#2ed573' :
                        this.state.value1 == 4 ? '#f6e58d' :
                          this.state.value1 == 5 ? '#ffbe76' :
                            this.state.value1 == 6 ? '#ffbe76' :
                              this.state.value1 == 7 ? '#ff9f43' :
                                this.state.value1 == 8 ? '#ff9f43' :
                                  this.state.value1 == 9 ? '#ff6b6b' :
                                    this.state.value1 == 10 ? '#e74c3c' : null}
                thumbButtonSize={30}
                sliderWidth={200}
                sliderBorderRadius={30}
                onValueChange={value1 => {
                  this.setState({ value1: parseInt(value1) })
                }}
                disabledHoverEffect={false}
              />
            </View>
            : null
          }

          {this.state.num == 1 ?
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{
                fontSize: 20,
                fontWeight: 'bold',
                textAlign: 'center',
                marginVertical: 40,
                marginHorizontal: 40
              }}>
                {this.state.value2} %
              </Text>

              <Slider
                value={this.state.value2}
                minimumValue={0}
                maximumValue={100}
                sliderHeight={20}
                thumbTintColor={'#1b263a'}
                minimumTrackTintColor={'#1b263a'}
                thumbButtonSize={30}
                sliderWidth={200}
                sliderBorderRadius={30}
                onValueChange={value2 => {
                  this.setState({ value2: parseInt(value2) })
                }}
                disabledHoverEffect={false}
              />
            </View>
            : null
          }

          {this.state.num == 2 ?
            <View>
              <View style={{
                backgroundColor: 'white',
                elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                justifyContent: "center",
                height: 40,
                width: 100,
                marginTop: 40,
                borderRadius: 40
              }}>
                <TextInput
                  value={this.state.tempo}
                  onChangeText={tempo => this.setState({ tempo })}
                  maxLength={4}
                  style={{ textAlign: 'center' }}
                  keyboardType='numeric'
                  returnKeyLabel='Done'
                  returnKeyType='done'
                  underlineColorAndroid='transparent' />
              </View>
              <Text
                style={{ textAlign: 'center', fontSize: 11 }}
              >Minutos</Text>
            </View>
            : null
          }
        </View>
      </View>
    )
  }
}

export default LerDepois;