import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  BackHandler,
  ImageBackground,
  Picker,
  Platform,
  ActivityIndicator,
  Modal,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'
import {vencido} from '../utils/consultaPlano'

class Calendario extends Component {
  state = {
    dia: '',
    diaAtual: new Date(),
    modalVisibleSenha: false,
    datasourse: [],
    colors: [
      '#2ed573',
    ]
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('ex')
      return true;
    })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then(async (contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].idUsuario),
            idDesk: String(JSON.parse(contents)[0].idDesk),
          })

          if(vencido(String(JSON.parse(contents)[0].idUsuario))){
            alert('Seu plano expirou, renove seu plano!')
            this.props.navigation.navigate('Financeiro')
            return false
          }

          this.setState({
            refresh: true
          })

          this.buscaPlano()

          const isConnected = await checkInternetConnection();
          if (isConnected) {

            fetch(urlAPI + '/calendario/get.php?tecnico=' + String(JSON.parse(contents)[0].tecnico) + '&atleta=' + String(JSON.parse(contents)[0].idDesk), {}
            ).then((response) => response.json())
              .then((responseJson) => {
                this.setState({
                  refresh: false
                })
                if (!responseJson.message) {
                  this.setState({
                    datasourse: responseJson.records,
                  })

                } else {
                  alert('Nenhum evento encontrado')
                  this.setState({
                    refresh: false
                  })
                }
              })
              .catch((error) => {
                alert('ocorreu um erro ' + error);
                this.setState({
                  refresh: false
                })
              });

          } else {
            alert('Sem conexão com a internet!')
            this.setState({
              refresh: false
            })
          }
        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );

          this.props.navigation.navigate('ex')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });

  }

  buscaPlano = async () => {
    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(urlAPI + '/planos/getativos.php?atleta=' + this.state.idUsuario, {}
      ).then((response) => response.json())
        .then((responseJson) => {
          if (!responseJson.message) {

          } else {
            alert('Plano expirado, ative seu plano.')
            this.props.navigation.navigate('Financeiro')
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });
    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  criaTreino = () => {
    let treinos = []

    for (let index = 0; index < this.state.datasourse.length; index++) {
      const element = this.state.datasourse[index];

      treinos.push(
        <View style={{
          height: 80,
          borderRadius: 10,
          backgroundColor: 'white',
          marginHorizontal: 20,
          marginVertical: 5,
        }}>
          <View style={{
            height: 20,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            justifyContent: 'center',
            backgroundColor: this.state.colors[Math.floor(Math.random() * this.state.colors.length)]
          }}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 10,
                color: 'white'
              }}>{String('Corrida').toLocaleUpperCase()}</Text>
          </View>

          <View>
            <Text style={{
              fontSize: 11,
              fontWeight: 'bold',
              marginHorizontal: 10,
              marginTop: 5
            }}>Evento</Text>
            <Text style={{
              fontSize: 10,
              marginHorizontal: 10,
            }}>{element.Nome}</Text>

            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
              <Text style={{
                fontSize: 10,
                marginHorizontal: 10,
              }}>{element.Data}</Text>

              <Text style={{
                fontSize: 10,
                marginHorizontal: 10,
              }}><Text style={{ fontWeight: 'bold' }}>RESULTADO: </Text>{(element.Resultado == '' | element.Resultado == '-') ? 'Não informado' : element.Resultado}</Text>
            </View>
          </View>
        </View>
      )

    }

    return treinos
  }

  /* tela principal */
  render() {
    return (
      <View style={{ backgroundColor: '#141b29', flex: 1 }}>
        <StatusBar
          barStyle='light-content'
          backgroundColor='#1b263a' />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
          height: Platform.OS == "android" ? 60 : 90,
          alignItems: 'flex-end',
          backgroundColor: '#1b263a'
        }}>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
          </TouchableOpacity>

          <Text style={{
            width: '33.33%',
            fontWeight: 'bold',
            fontSize: 16,
            color: 'white',
            marginBottom: Platform.OS == "android" ? 8 : 13,
            marginLeft: 20
          }}>MMS ATLETA</Text>

          <View style={{ width: '33.33%' }} />
        </View>

        <ScrollView style={{
          flex: 1,
          marginTop: 20
        }}>
          {this.state.refresh ?
            <ActivityIndicator
              color={"white"}
              size="large"
            />
            :
            this.criaTreino()}
        </ScrollView>

      </View>
    )
  }
}

export default Calendario;