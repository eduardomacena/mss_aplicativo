import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Button,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Image,
    Alert,
    ImageBackground,
    ActivityIndicator,
    Picker,
    Platform,
    Modal,
} from 'react-native';
import Slider from 'react-native-simple-slider';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'

class Cadastro extends Component {
    state = {
        num: 0,
        value1: 0,
        value2: 0,
        tempo: 0,
        items: [
            { frase: 'Qual é o seu nome completo?' },
            // { frase: 'Qual é a sua data de nascimento?' },
            { frase: 'Qual é o seu número de celular?' },
            { frase: 'Qual é o seu e-mail?' },
            //  { frase: 'Qual é o número do seu documento CPF?' },
            { frase: 'Qual é o seu peso atual em kg?' },
            { frase: 'Qual é a sua altura em metros?' },
            { frase: 'Quais são seus principais objetivos?' },
           // { frase: 'Qual é a data da sua principal competição?' },
            { frase: 'Você ja treina?' },
            { frase: 'Qual é a sua média de KM semanal?' },
            { frase: 'Onde costuma treinar? O local possui marcação de distância?' },
            { frase: 'Quantos dias disponíveis para treinar na semana (3,4,5,6 ou 7)?' },
            { frase: 'Quanto tempo disponível por dia?' },
            { frase: 'Já fez alguma cirurgia?' },
            { frase: 'Possui alguma doença? Pressão Alta? Diabetes? Caso não tenha apenas digite "Não"' },
            { frase: 'Toma algum remédio? se "Sim" escreva o nome do mesmo, se não, apenas escreva no campo "Não"' },
            { frase: 'Sente Dores? Descreva-as.' }
        ],
        modalVisibleSenha: false,
        nome: "",
        dataNasci: "",
        cel: "",
        email: "",
        cpf: "",
        objetivo: "",
        peso: "",
        altura: "",
        treina: "",
        metaKM: "",
        local: "",
        diasDisponiveis: "",
        tempo: "",
        cirurgia: "",
        doenca: "",
        remedio: "",
        dores: ""
    }

    avancaFrase = () => {
        this.setState({
            num: this.state.num == (this.state.items.length - 1) ? this.state.num : (this.state.num + 1)
        })
    }

    retroFrase = () => {
        this.setState({
            num: this.state.num == 0 ? 0 : (this.state.num - 1)
        })
    }

    enviaQuestionario = async () => {
        this.setState({
            refresh: false
        })

        if (this.state.nome == "") {
            alert("Volte, Preencha o campo nome completo")
            return
        }

        if (this.state.cel == "") {
            alert("Volte, Preencha o campo celular")
            return
        }

        if (this.state.email == "") {
            alert("Volte, Preencha o campo email")
            return
        }

        const isConnected = await checkInternetConnection();
        if (isConnected) {

            let dados = {
                "Atleta": "",
                "Senha": "",
                "Pagamento": 0,
                "NomeCompleto": this.state.nome,
                "Nascimento": this.state.dataNasci,
                "Celular": this.state.cel,
                "Email": this.state.email,
                "Cpf": "",
                "Objetivo1": this.state.objetivo,
                "Objetivo2": "",
                "Peso": this.state.peso,
                "Altura": this.state.altura,
                "Treina": this.state.treina,
                "MediaKm": this.state.metaKM,
                "Local": this.state.local,
                "InfoTreino": "",
                "Dias": this.state.diasDisponiveis,
                "Tempo": this.state.tempo,
                "Cirurgia": this.state.cirurgia,
                "Doenca": this.state.doenca,
                "Remedio": this.state.remedio,
                "Dores": this.state.dores,
                "IdDesk": 0
            }

            fetch(urlAPI + '/atletas/post.php', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(dados)
            }
            ).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.message == 'usuario criado') {
                        alert('Suas informações foram enviadas com sucesso, agora só aguardar o contato do treinador!')
                        this.props.navigation.navigate('Login')
                    } else {
                        alert('Falha no servidor, entre em contato com o suporte!')
                        this.props.navigation.navigate('Login')
                    }

                })
                .catch((error) => {
                    alert('ocorreu um erro ' + JSON.stringify(error));
                    this.setState({
                        refresh: false
                    })
                });
        } else {
            alert('Sem conexão com a internet!')
            this.setState({
                refresh: false
            })
        }
    }

    cpf = (b) => {
        var v = b
        v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
        v = v.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
        v = v.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
        v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
        // v = v.replace(/\.(\d{3})(\d)/, ".$1/$2"); //Coloca uma barra entre o oitavo e o nono dígitos
        //v = v.replace(/(\d{4})(\d)/, "$1-$2"); //Coloca um hífen depois do bloco de quatro dígitos
        return v;
    }

    dianum = (cel) => {
        var v = cel
        v = v.replace(/\D/g, "");
        v = v.replace(/(\d{2})(\d)/, "$1/$2");
        v = v.replace(/(\d{2})(\d{1,2})/, "$1/$2");
        return v
    }

    fone = (cel) => {
        var v = cel
        v = v.replace(/\D/g, "");
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
        v = v.replace(/(\d)(\d{4})$/, "$1-$2");
        return v
    }

    /* tela principal */
    render() {
        return (
            this.state.refresh ?
                <ActivityIndicator
                    size="large"
                    color="white"
                    style={{
                        flex: 1,
                        backgroundColor: "#1b263a"
                    }}
                />
                :
                <View style={{ backgroundColor: '#1b263a', flex: 1 }}>
                    <StatusBar
                        barStyle='light-content'
                        backgroundColor='#1b263a' />

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            elevation: 3,
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 1 },
                            shadowOpacity: 0.5,
                            shadowRadius: 2,
                            flex: 1,
                            marginTop: 20,
                            backgroundColor: '#1b263a'
                        }}>
                        <View onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
                        </View>

                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 16,
                            marginTop: 12,
                            color: 'white',
                            marginLeft: -15
                        }}></Text>

                        <View style={{ width: '33.33%' }} />
                    </View>

                    <View style={{ flex: 1, justifyContent: 'space-between', backgroundColor: 'white' }}>
                        <View />
                        <View
                            style={{
                                flexDirection: 'row',
                                marginBottom: 20,
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                marginHorizontal: 20
                            }}>
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.state.num == 0) {
                                        this.props.navigation.navigate('Login')
                                    } else {
                                        this.retroFrase()
                                    }
                                }}
                                style={{
                                    height: 50,
                                    width: 100,
                                    justifyContent: 'center',
                                    borderRadius: 40,
                                    backgroundColor: '#1b263a'
                                }}>
                                <Text style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontSize: 11
                                }}>VOLTAR</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {

                                    if (this.state.num == 14) {
                                        this.enviaQuestionario()
                                    } else {
                                        this.avancaFrase()
                                    }

                                }}
                                style={{
                                    height: 50,
                                    width: 100,
                                    justifyContent: 'center',
                                    borderRadius: 40,
                                    backgroundColor: '#2ecc71'
                                }}>
                                <Text style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    fontSize: 11
                                }}>{this.state.num == 14 ? 'CONCLUIR' : 'PRÓXIMO'}</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                    <View
                        style={{
                            position: 'absolute',
                            height: '60%',
                            width: '80%',
                            alignSelf: 'center',
                            justifyContent: 'center',
                            borderRadius: 20,
                            marginTop: '30%',
                            alignItems: 'center',
                            backgroundColor: 'white',
                            elevation: 3,
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 1 },
                            shadowOpacity: 0.5,
                            shadowRadius: 2,
                        }}>
                        <Text
                            style={{
                                fontSize: 20,
                                fontWeight: 'bold',
                                textAlign: 'center',
                                marginHorizontal: 40
                            }}>
                            {this.state.items[this.state.num].frase}
                        </Text>


                        {this.state.num == 0 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    justifyContent: "center",
                                    alignSelf: 'center',
                                    height: 40,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.nome}
                                        placeholder='João Silva'
                                        onChangeText={nome => this.setState({ nome })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='default'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Nome Completo</Text>
                            </View>
                            : null
                        }

                        {/*this.state.num == 1 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                                    alignSelf: 'center',
                                    height: 40,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.dataNasci}
                                        placeholder='99/99/9999'
                                        onChangeText={dataNasci => this.setState({ dataNasci: this.dianum(dataNasci) })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                returnKeyLabel='Done'
                returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Data Nascimento</Text>
                            </View>
                            : null
                        */}

                        {this.state.num == 1 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.cel}
                                        placeholder='(99) 9 9999-9999'
                                        onChangeText={cel => this.setState({ cel: this.fone(cel) })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                                        returnKeyLabel='Done'
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Celular</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 2 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.email}
                                        placeholder='email@dominio.com'
                                        onChangeText={email => this.setState({ email })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='email-address'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>E-mail</Text>
                            </View>
                            : null
                        }

                        {/*this.state.num == 4 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                                    alignSelf: 'center',
                                    height: 40,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.cpf}
                                        placeholder='999.999.999-99'
                                        onChangeText={cpf => this.setState({ cpf: this.cpf(cpf) })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                returnKeyLabel='Done'
                returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>CPF</Text>
                            </View>
                            : null
                        */}

                        {this.state.num == 3 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 100,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.peso}
                                        placeholder='99'
                                        maxLength={3}
                                        onChangeText={peso => this.setState({ peso })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                                        returnKeyLabel='Done'
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>PESO(kg)</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 4 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 100,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.altura}
                                        placeholder='1,50'
                                        maxLength={4}
                                        onChangeText={altura => this.setState({ altura })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                                        returnKeyLabel='Done'
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Altura(mt)</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 5 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 80,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.objetivo}
                                        placeholder='Descreva'
                                        maxLength={180}
                                        onChangeText={objetivo => this.setState({ objetivo })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='default'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Objetivo</Text>
                            </View>
                            : null
                        }

                        {/*this.state.num == 6 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 100,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.principalEve}
                                        placeholder='99/99/9999'
                                        maxLength={4}
                                        onChangeText={principalEve => this.setState({ principalEve })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                                        returnKeyLabel='Done'
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Data</Text>
                            </View>
                            : null
                        */}

                        {this.state.num == 6 ?
                            <View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ treina: "SIM" }) }}
                                        style={{
                                            backgroundColor: this.state.treina == "SIM" ? '#1b263a' : 'white',
                                            elevation: 3,
                                            shadowColor: '#000',
                                            shadowOffset: { width: 0, height: 1 },
                                            shadowOpacity: 0.5,
                                            shadowRadius: 2,
                                            shadowColor: '#000',
                                            shadowOffset: { width: 0, height: 1.5 },
                                            shadowOpacity: 0.5,
                                            shadowRadius: 2,
                                            alignSelf: 'center',
                                            justifyContent: 'center',
                                            height: 40,
                                            width: 70,
                                            marginTop: 40,
                                            borderRadius: 40
                                        }}>
                                        <Text
                                            style={{
                                                textAlign: 'center',
                                                color: this.state.treina == "SIM" ? 'white' : 'black'
                                            }}>SIM</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => { this.setState({ treina: "NAO" }) }}
                                        style={{
                                            backgroundColor: this.state.treina == "NAO" ? '#1b263a' : 'white',
                                            justifyContent: 'center',
                                            elevation: 3,
                                            shadowColor: '#000',
                                            shadowOffset: { width: 0, height: 1 },
                                            shadowOpacity: 0.5,
                                            shadowRadius: 2,
                                            alignSelf: 'center',
                                            height: 40,
                                            marginLeft: 20,
                                            width: 70,
                                            marginTop: 40,
                                            borderRadius: 40
                                        }}>
                                        <Text
                                            style={{
                                                textAlign: 'center',
                                                color: this.state.treina == "NAO" ? 'white' : 'black'
                                            }}>NÃO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : null
                        }

                        {this.state.num == 7 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 100,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.metaKM}
                                        placeholder='99'
                                        maxLength={2}
                                        onChangeText={metaKM => this.setState({ metaKM })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                                        returnKeyLabel='Done'
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>KM</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 8 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 120,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.local}
                                        placeholder='Parque'
                                        maxLength={80}
                                        onChangeText={local => this.setState({ local })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='email-address'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>KM</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 9 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 100,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.diasDisponiveis}
                                        placeholder='0'
                                        maxLength={1}
                                        onChangeText={diasDisponiveis => this.setState({ diasDisponiveis })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                                        returnKeyLabel='Done'
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>dias</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 10 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 100,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.tempoDia}
                                        placeholder='0'
                                        maxLength={3}
                                        onChangeText={tempoDia => this.setState({ tempoDia })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='numeric'
                                        returnKeyLabel='Done'
                                        returnKeyType='done'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>minutos</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 11 ?
                            <View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ cirurgia: "SIM" }) }}
                                        style={{
                                            backgroundColor: this.state.cirurgia == "SIM" ? '#1b263a' : 'white',
                                            elevation: 3,
                                            shadowColor: '#000',
                                            shadowOffset: { width: 0, height: 1 },
                                            shadowOpacity: 0.5,
                                            shadowRadius: 2,
                                            alignSelf: 'center',
                                            justifyContent: 'center',
                                            height: 40,
                                            width: 70,
                                            marginTop: 40,
                                            borderRadius: 40
                                        }}>
                                        <Text
                                            style={{
                                                textAlign: 'center',
                                                color: this.state.cirurgia == "SIM" ? 'white' : 'black'
                                            }}>SIM</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => { this.setState({ cirurgia: "NAO" }) }}
                                        style={{
                                            backgroundColor: this.state.cirurgia == "NAO" ? '#1b263a' : 'white',
                                            justifyContent: 'center',
                                            elevation: 3,
                                            shadowColor: '#000',
                                            shadowOffset: { width: 0, height: 1 },
                                            shadowOpacity: 0.5,
                                            shadowRadius: 2,
                                            alignSelf: 'center',
                                            height: 40,
                                            marginLeft: 20,
                                            width: 70,
                                            marginTop: 40,
                                            borderRadius: 40
                                        }}>
                                        <Text
                                            style={{
                                                textAlign: 'center',
                                                color: this.state.cirurgia == "NAO" ? 'white' : 'black'
                                            }}>NÃO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : null
                        }

                        {this.state.num == 12 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.doenca}
                                        placeholder='Doença'
                                        onChangeText={doenca => this.setState({ doenca })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='default'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Doença</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 13 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 40,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.remedio}
                                        placeholder='Remédio'
                                        onChangeText={remedio => this.setState({ remedio })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='default'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Remédio</Text>
                            </View>
                            : null
                        }

                        {this.state.num == 14 ?
                            <View>
                                <View style={{
                                    backgroundColor: 'white',
                                    elevation: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.5,
                                    shadowRadius: 2,
                                    alignSelf: 'center',
                                    justifyContent: "center",
                                    height: 80,
                                    width: 200,
                                    marginTop: 40,
                                    borderRadius: 40
                                }}>
                                    <TextInput
                                        value={this.state.dores}
                                        placeholder='Descreva'
                                        //maxLength={180}
                                        onChangeText={dores => this.setState({ dores })}
                                        style={{ textAlign: 'center' }}
                                        keyboardType='default'
                                        underlineColorAndroid='transparent' />
                                </View>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontSize: 11,
                                    }}>Dores</Text>
                            </View>
                            : null
                        }
                    </View>
                </View>
        )
    }
}

export default Cadastro;