import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  ImageBackground,
  Platform,
  NetInfo,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';

class Principal extends Component {
  state = {
    emailS: '',
    email: '',
    senha: '',
    idUsuario: '',
    modalVisibleSenha: false,
  }

  static navigationOptions = {
    header: null,
  };

  /* tela principal */
  render() {
    return (
      <View style={{ backgroundColor: '#141b29', flex: 1 }}>
        <StatusBar
          barStyle='light-content'
          backgroundColor='#1b263a' />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
          height: Platform.OS == "android" ? 60 : 90,
          alignItems: 'flex-end',
          backgroundColor: '#1b263a'
        }}>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
          </TouchableOpacity>

          <Text style={{
            width: '33.33%',
            fontWeight: 'bold',
            fontSize: 16,
            color: 'white',
            marginBottom: Platform.OS == "android" ? 8 : 13,
            marginLeft: 20
          }}>MMS ATLETA</Text>

          <View style={{ width: '33.33%' }} />
        </View>
        <ScrollView>
          <View style={{
            borderLeftWidth: 5,
            borderColor: '#0be881',
            borderRadius: 5,
            paddingRight: 10,
            backgroundColor: 'white',
            marginHorizontal: 30,
            marginTop: 30,
            marginBottom: 10
          }}>
            <Text style={{
              color: '#1b263a',
              fontSize: 11,
              textAlign: 'right',
              fontWeight: 'bold',
              marginLeft: 5
            }}>AQUECIMENTO</Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              const RNFS = require('react-native-fs');
              const cadastro = RNFS.DocumentDirectoryPath + '/aquecimento.json';

              let dados = [];

              dados.push({
                "aquecimento": "Alongamento1",
              })

              RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
                .then((success) => {
                  console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                  console.log(err.message);
                });

              this.props.navigation.navigate('Aquecimento')
            }}>
            <ImageBackground
              source={require('../img/alongamento1.jpg')}
              imageStyle={{ borderRadius: 15, }}
              style={{
                height: 150,
                marginHorizontal: 25,
              }}>
              <View style={{ alignSelf: 'flex-end' }}>
                <Text style={{
                  fontWeight: 'bold',
                  textAlign: 'right',
                  fontSize: 16,
                  width: 150,
                  marginRight: 20,
                  marginTop: 20,
                  color: 'white'
                }}>
                  AQUECIMENTO 1
            </Text>

                <View style={{ flexDirection: 'row', marginHorizontal: 20, alignItems: 'center', alignSelf: 'flex-end' }}>
                  <Icon name="clockcircle" color='white' size={10} />
                  <Text style={{ fontSize: 10, marginLeft: 5, color: 'white' }}>5 MINUTOS</Text>
                </View>

              </View>
            </ImageBackground>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              const RNFS = require('react-native-fs');
              const cadastro = RNFS.DocumentDirectoryPath + '/aquecimento.json';

              let dados = [];

              dados.push({
                "aquecimento": "Alongamento2",
              })

              RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
                .then((success) => {
                  console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                  console.log(err.message);
                });
              this.props.navigation.navigate('Aquecimento')
            }}
            style={{
              marginTop: 15
            }}>
            <ImageBackground
              source={require('../img/alongamento2.jpg')}
              imageStyle={{ borderRadius: 15, }}
              style={{
                height: 150,
                marginHorizontal: 25
              }}>
              <View style={{ alignSelf: 'flex-start' }}>
                <Text style={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  width: 150,
                  marginLeft: 20,
                  marginTop: 80,
                  color: 'white'
                }}>
                  AQUECIMENTO 2
            </Text>

                <View style={{
                  flexDirection: 'row',
                  marginHorizontal: 20,
                  alignItems: 'center',
                  alignSelf: 'flex-start'
                }}>
                  <Icon name="clockcircle" color='white' size={10} />
                  <Text style={{ fontSize: 10, marginLeft: 5, color: 'white' }}>5 MINUTOS</Text>
                </View>

              </View>

            </ImageBackground>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginTop: 15
            }}
            onPress={() => {
              const RNFS = require('react-native-fs');
              const cadastro = RNFS.DocumentDirectoryPath + '/aquecimento.json';

              let dados = [];

              dados.push({
                "aquecimento": "Alongamento3",
              })

              RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
                .then((success) => {
                  console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                  console.log(err.message);
                });

              this.props.navigation.navigate('Aquecimento')
            }}>
            <ImageBackground
              source={require('../img/alongamento3.jpg')}
              imageStyle={{ borderRadius: 15, }}
              style={{
                height: 150,
                marginHorizontal: 25,
              }}>
              <View style={{ alignSelf: 'flex-start' }}>
                <Text style={{
                  fontWeight: 'bold',
                  textAlign: 'right',
                  fontSize: 16,
                  width: 150,
                  marginRight: 20,
                  marginTop: 20,
                  color: 'white'
                }}>AQUECIMENTO 3</Text>

                <View style={{ flexDirection: 'row', marginHorizontal: 20, alignItems: 'center', alignSelf: 'flex-end' }}>
                  <Icon name="clockcircle" color='white' size={10} />
                  <Text style={{ fontSize: 10, marginLeft: 5, color: 'white' }}>4 MINUTOS</Text>
                </View>

              </View>
            </ImageBackground>
          </TouchableOpacity>

          <View style={{
            borderLeftWidth: 4,
            borderColor: '#0be881',
            borderRadius: 5,
            paddingRight: 10,
            backgroundColor: 'white',
            marginHorizontal: 30,
            marginTop: 45,
            marginBottom: 10
          }}>
            <Text style={{
              color: '#1b263a',
              fontSize: 11,
              textAlign: 'right',
              fontWeight: 'bold',
              marginLeft: 5
            }}>TÉCNICAS</Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              const RNFS = require('react-native-fs');
              const cadastro = RNFS.DocumentDirectoryPath + '/aquecimento.json';

              let dados = [];

              dados.push({
                "aquecimento": "Tecnicas1",
              })

              RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
                .then((success) => {
                  console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                  console.log(err.message);
                });

              this.props.navigation.navigate('Aquecimento')
            }}>
            <ImageBackground
              source={require('../img/tecnica1.jpg')}
              imageStyle={{ borderRadius: 15, }}
              style={{
                height: 150,
                marginHorizontal: 25
              }}>
              <View style={{ alignSelf: 'flex-end' }}>
                <Text style={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  textAlign: 'right',
                  width: 150,
                  marginRight: 20,
                  marginTop: 20,
                  color: 'white'
                }}>
                  TÉCNICA 1
            </Text>

                <View style={{
                  flexDirection: 'row',
                  marginHorizontal: 20,
                  alignItems: 'center',
                  alignSelf: 'flex-end'
                }}>
                  <Icon name="clockcircle" color='white' size={10} />
                  <Text style={{ fontSize: 10, marginLeft: 5, color: '#dfe6e9' }}>8 MINUTOS</Text>
                </View>

              </View>

            </ImageBackground>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              const RNFS = require('react-native-fs');
              const cadastro = RNFS.DocumentDirectoryPath + '/aquecimento.json';

              let dados = [];

              dados.push({
                "aquecimento": "Tecnicas2",
              })

              RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
                .then((success) => {
                  console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                  console.log(err.message);
                });

              this.props.navigation.navigate('Aquecimento')
            }} style={{
              marginTop: 15
            }}>
            <ImageBackground
              source={require('../img/tecnica2.jpg')}
              imageStyle={{ borderRadius: 15, }}
              style={{
                height: 150,
                marginHorizontal: 25
              }}>
              <View style={{ alignSelf: 'flex-start' }}>
                <Text style={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  width: 150,
                  marginLeft: 20,
                  marginTop: 20,
                  color: 'black'
                }}>
                  TÉCNICA 2
            </Text>

                <View style={{
                  flexDirection: 'row',
                  marginHorizontal: 20,
                  alignItems: 'center',
                  alignSelf: 'flex-start'
                }}>
                  <Icon name="clockcircle" color='black' size={10} />
                  <Text style={{ fontSize: 10, marginLeft: 5 }}>8 MINUTOS</Text>
                </View>

              </View>
            </ImageBackground>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              const RNFS = require('react-native-fs');
              const cadastro = RNFS.DocumentDirectoryPath + '/aquecimento.json';

              let dados = [];

              dados.push({
                "aquecimento": "Tecnicas3",
              })

              RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
                .then((success) => {
                  console.log('FILE WRITTEN!');
                })
                .catch((err) => {
                  console.log(err.message);
                });

              this.props.navigation.navigate('Aquecimento')
            }} style={{
              marginTop: 15,
              marginBottom: 20
            }}>
            <ImageBackground
              source={require('../img/tecnica3.jpg')}
              imageStyle={{ borderRadius: 15, }}
              style={{
                height: 150,
                marginHorizontal: 25
              }}>
              <View style={{
                alignSelf: 'flex-end',
                justifyContent: 'flex-start',
                height: '100%',
                marginBottom: 20
              }}>
                <Text style={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  width: 100,
                  marginLeft: 20,
                  marginTop: 10,
                  color: 'black'
                }}>TÉCNICA 3</Text>

                <View style={{
                  flexDirection: 'row',
                  marginHorizontal: 20,
                  alignItems: 'center',
                  alignSelf: 'flex-start'
                }}>
                  <Icon name="clockcircle" color='black' size={10} />
                  <Text style={{ fontSize: 10, marginLeft: 5 }}>8 MINUTOS</Text>
                </View>

              </View>
            </ImageBackground>
          </TouchableOpacity>

        </ScrollView>
      </View>
    )
  }
}

export default Principal;