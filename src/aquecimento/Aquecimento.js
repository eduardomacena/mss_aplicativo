import React, { Component } from 'react'
import {
    View,
    ScrollView,
    Button,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Image,
    Alert,
    ImageBackground,
    Picker,
    Platform,
    BackHandler,
    Dimensions,
    ActivityIndicator,
    Modal,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'
import VideoPlayer from 'react-native-video-player';

export default class Aquecimento extends Component {

    constructor() {
        super();

        this.state = {
            orientation: '',
            video: {
                width: undefined,
                height: undefined,
                duration: undefined
            },
            thumbnailUrl: undefined,
            videoUrl: undefined,
            descricao: ''
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ex')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/aquecimento.json';

        RNFS.readFile(path, 'utf8')
            .then(async (contents) => {
                if (!(contents == '[{}]')) {
                    if ((await this.testeLogin())) {
                        this.buscaAquecimento(JSON.parse(contents)[0].aquecimento)
                    } else {

                        if (
                            !((JSON.parse(contents)[0].aquecimento == 'Alongamento3') |
                                (JSON.parse(contents)[0].aquecimento == 'Tecnicas3'))
                        ) {
                            this.buscaAquecimento(JSON.parse(contents)[0].aquecimento)
                        } else {
                            Alert.alert(
                                'Confirmação',
                                'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                                [
                                    { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                    { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                                ]
                            );

                            this.props.navigation.navigate('ex')
                            this.setState({ idUsuario: '0' })
                        }
                    }
                } else {
                    alert('Nada foi encontrado')

                    this.props.navigation.navigate('ex')
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            });

        this.getOrientation();

        Dimensions.addEventListener('change', () => {
            this.getOrientation();
        });
    }

    getOrientation = () => {
        if (Dimensions.get('window').width < Dimensions.get('window').height) {
            this.setState({ orientation: 'portrait' });
        }
        else {
            this.setState({ orientation: 'landscape' });
        }
    }

    buscaAquecimento = async (aquecimento) => {
        this.setState({
            refresh: true
        })

        const isConnected = await checkInternetConnection();
        if (isConnected) {

            fetch(urlAPI + '/exercicios/get_aquecimento.php?aquecimento=' + aquecimento, {}
            ).then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        refresh: false
                    })

                    if (!responseJson.message) {

                        global.fetch(`https://player.vimeo.com/video/${responseJson.records[0].ngif}/config`)
                            .then(res => res.json())
                            .then(res => this.setState({
                                thumbnailUrl: res.video.thumbs['640'],
                                videoUrl: res.request.files.hls.cdns[res.request.files.hls.default_cdn].url,
                                video: res.video,
                                descricao: (aquecimento == 'Alongamento1' ? 'Aquecimento 1' :
                                    aquecimento == 'Alongamento2' ? 'Aquecimento 2' :
                                        aquecimento == 'Alongamento3' ? 'Aquecimento 3' :
                                            aquecimento == 'Tecnicas1' ? 'Técnica 1' :
                                                aquecimento == 'Tecnicas2' ? 'Técnica 2' :
                                                    aquecimento == 'Tecnicas3' ? 'Técnica 3' : '')
                            }));

                    } else {
                        alert(JSON.stringify(responseJson))
                        this.setState({
                            refresh: false
                        })
                    }
                })
                .catch((error) => {
                    alert('ocorreu um erro ' + error);
                    this.setState({
                        refresh: false
                    })
                });

        } else {
            alert('Sem conexão com a internet!')
        }
    }

    testeLogin = async () => {
        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';

        let result;

        await RNFS.readFile(path, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {
                    result = true
                } else {
                    result = false
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            })

        return result
    }

    render() {
        return (

            (this.state.orientation == 'portrait') ?

                <View style={{ backgroundColor: '#141b29', flex: 1 }}>
                    <StatusBar
                        barStyle='light-content'
                        backgroundColor='#1b263a' />

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                        height: Platform.OS == "android" ? 60 : 90,
                        alignItems: 'flex-end',
                        backgroundColor: '#1b263a'
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
                            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
                        </TouchableOpacity>

                        <Text style={{
                            width: '33.33%',
                            fontWeight: 'bold',
                            fontSize: 16,
                            color: 'white',
                            marginBottom: Platform.OS == "android" ? 8 : 13,
                            marginLeft: 20
                        }}>MMS ATLETA</Text>

                        <View style={{ width: '33.33%' }} />
                    </View>

                    <View
                        style={{
                            flex: 1,
                            margin: 20,
                            borderRadius: 20,
                            //backgroundColor: 'white',
                            justifyContent: "space-between"
                        }}>
                        <Text style={{
                            textAlign: "center",
                            fontSize: 25,
                            fontWeight: "bold",
                            marginHorizontal: '15%',
                            marginTop: 30,
                            color: 'white'
                        }}>{this.state.descricao}</Text>

                        <VideoPlayer
                            endWithThumbnail
                            thumbnail={{ uri: this.state.thumbnailUrl }}
                            video={{ uri: this.state.videoUrl }}
                            videoWidth={this.state.video.width}
                            videoHeight={this.state.video.height}
                            duration={this.state.video.duration/* I'm using a hls stream here, react-native-video
                               can't figure out the length, so I pass it here from the vimeo config */}
                            ref={r => this.player = r}
                        />
                        <View />
                    </View>
                </View>

                :

                <VideoPlayer
                    endWithThumbnail
                    thumbnail={{ uri: this.state.thumbnailUrl }}
                    video={{ uri: this.state.videoUrl }}
                    videoWidth={this.state.video.width}
                    videoHeight={this.state.video.height}
                    duration={this.state.video.duration/* I'm using a hls stream here, react-native-video
                               can't figure out the length, so I pass it here from the vimeo config */}
                    ref={r => this.player = r}
                />
        )
    }
}

const styles = StyleSheet.create({})
