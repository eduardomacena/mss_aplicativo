import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  ImageBackground,
  Picker,
  Platform,
  BackHandler,
  Modal,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';

class Perfil extends Component {
  state = {
    idUsuario: "",
    nome: "",
    email: "",
    celular: "",
    altura: "",
    peso: "",
    mediakm: ""
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('ex')
      return true;
    })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {


          this.setState({
            idUsuario: String(JSON.parse(contents)[0].idUsuario),
            nome: String(JSON.parse(contents)[0].nome),
            email: String(JSON.parse(contents)[0].email),
            celular: this.mtel(String(JSON.parse(contents)[0].celular)),
            altura: String(JSON.parse(contents)[0].altura),
            peso: String(JSON.parse(contents)[0].peso),
            mediakm: String(JSON.parse(contents)[0].mediakm)
          })

        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );

          this.props.navigation.navigate('ex')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });

  }

  static navigationOptions = {
    header: null,
  };

  mtel = (v) => {
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

  /* tela principal */
  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <StatusBar
          barStyle='light-content'
          backgroundColor='#1b263a' />

        <View
          style={{
            elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
            height: 280,
            backgroundColor: '#1b263a'
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              height: Platform.OS == "android" ? 60 : 90,
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
              <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
            </TouchableOpacity>

            <Text style={{
              width: '33.33%',
              fontWeight: 'bold',
              fontSize: 16,
              color: 'white',
              marginBottom: Platform.OS == "android" ? 8 : 13,
              marginLeft: 20
            }}></Text>

            <View style={{ width: '33.33%' }} />
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View />

            <View>
              <TouchableOpacity
                style={{
                  height: 80,
                  width: 80,
                  borderRadius: 30,
                  justifyContent: 'center',
                  backgroundColor: 'white',
                }}>
                <IconF name="user" color='black' style={{ alignSelf: 'center' }} size={50} />
              </TouchableOpacity>
              <Text
                style={{
                  marginTop: 15,
                  color: 'white',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  fontSize: 12,
                }}>{this.state.nome}</Text>
              <Text
                style={{
                  fontSize: 10,
                  textAlign: 'center',
                  color: 'white',
                }}>Atleta</Text>
            </View>

            <View />
          </View>

          <View
            style={{
              height: 80,
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>

            <View
              style={{
                marginLeft: 30,
              }}>
              <Text
                style={{
                  fontSize: 11,
                  textAlign: 'center',
                  color: 'white',
                }}>{this.state.mediakm} KM</Text>

              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  fontSize: 11,
                }}>KM Semana</Text>
            </View>

            <View
              style={{
                width: 0.8,
                backgroundColor: 'white',
                height: '80%'
              }}
            />

            <View>
              <Text
                style={{
                  fontSize: 11,
                  textAlign: 'center',
                  color: 'white',
                }}>{this.state.altura} MT</Text>

              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  fontSize: 11,
                }}>Altura</Text>
            </View>

            <View
              style={{
                width: 0.8,
                backgroundColor: 'white',
                height: '80%'
              }}
            />

            <View
              style={{
                marginRight: 60,
              }}>
              <Text
                style={{
                  fontSize: 11,
                  textAlign: 'center',
                  color: 'white',
                }}>{this.state.peso} kg</Text>

              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  fontSize: 11,
                }}>Peso</Text>
            </View>

          </View>
        </View>

        <ScrollView contentContainerStyle={{
          flex: 1,
          marginTop: 20
        }}>

          <TouchableOpacity
            style={{
              height: 70,
              backgroundColor: 'white',
              elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
              marginTop: 2,
              marginHorizontal: 10,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
            <View
              style={{
                backgroundColor: '#7bed9f',
                width: 70,
                justifyContent: 'center',
                height: '100%',
              }}
            >
              <Iconr name="contacts" color='white' style={{ alignSelf: 'center' }} size={20} />
            </View>

            <View
              style={{
                flex: 1,
                paddingLeft: 10,
                justifyContent: 'center'
              }}>
              <Text
                style={[styles.TextTitle,
                { fontWeight: 'bold' }]}>{this.state.nome}</Text>
              <Text
                style={styles.TextTitle}>{this.state.email}</Text>
              <Text
                style={styles.TextTitle}>{this.state.celular}</Text>
            </View>

            <View
              style={{
                backgroundColor: '#7bed9f',
                width: 4,
                height: '100%',
              }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('TrocaSenha') }}
            style={{
              height: 70,
              backgroundColor: 'white',
              elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
              marginTop: 10,
              marginHorizontal: 10,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
            <View
              style={{
                backgroundColor: '#eccc68',
                width: 70,
                justifyContent: 'center',
                height: '100%',
              }}
            >
              <IconF name="key" color='white' style={{ alignSelf: 'center' }} size={20} />
            </View>

            <View
              style={{
                flex: 1,
                paddingLeft: 10,
                justifyContent: 'center'
              }}>
              <Text
                style={[styles.TextTitle,
                { fontWeight: 'bold' }]}>Trocar Senha</Text>
              <Text
                style={styles.TextTitle}>Toque para trocar a sua senha</Text>
            </View>

            <View
              style={{
                backgroundColor: '#eccc68',
                width: 4,
                height: '100%',
              }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              height: 70,
              backgroundColor: 'white',
              elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
              marginTop: 10,
              marginHorizontal: 10,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
            <View
              style={{
                backgroundColor: '#70a1ff',
                width: 70,
                justifyContent: 'center',
                height: '100%',
              }}
            >
              <Icon name="customerservice" color='white' style={{ alignSelf: 'center' }} size={20} />
            </View>

            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                paddingLeft: 10
              }}>
              <Text
                style={[styles.TextTitle,
                { fontWeight: 'bold' }]}>Suporte</Text>
              <Text
                style={styles.TextTitle}>mms@mmssystem.com.br</Text>
            </View>

            <View
              style={{
                backgroundColor: '#70a1ff',
                width: 4,
                height: '100%',
              }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('Login') }}
            style={{
              height: 70,
              backgroundColor: 'white',
              elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
              marginTop: 10,
              marginHorizontal: 10,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
            <View
              style={{
                backgroundColor: '#ff4757',
                width: 70,
                justifyContent: 'center',
                height: '100%',
              }}
            >
              <Iconr name="exit-run" color='white' style={{ alignSelf: 'center' }} size={20} />
            </View>

            <View
              style={{
                flex: 1,
                paddingLeft: 10,
                justifyContent: 'center'
              }}>
              <Text
                style={[styles.TextTitle,
                { fontWeight: 'bold' }]}>Logout</Text>
              <Text
                style={styles.TextTitle}>Toque para fazer logout da conta</Text>
            </View>

            <View
              style={{
                backgroundColor: '#ff4757',
                width: 4,
                height: '100%',
              }}
            />
          </TouchableOpacity>

        </ScrollView>

      </View>
    )
  }
}

export default Perfil;
const styles = StyleSheet.create({
  TextTitle: {
    marginLeft: 0,
    fontSize: 12
  }

});