import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Alert,
  Text,
  Dimensions,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import Principal from './principal/Principal';
import Exercicio from './exercicos/Exercicio';
import Planilha from './planilha/Planilha';
import Calendario from './calendario/Calendario';
import Financa from './financeiro/Financa'
import Planos from './financeiro/Planos'
import CartaoCredito from './financeiro/pagamentoAddCartao'
import LerAntes from './frases/LerAntes';
import LerDepois from './frases/LerDepois';
import Login from './login/Login';
import TrocaSenha from './login/AlteraSenha';
import Perfil from './perfil/Perfil';
import Cadastro from './login/Cadastro';
import Aquecimento from './aquecimento/Aquecimento';
import Teste from './teste/Teste'
import { createStackNavigator, navigation, createDrawerNavigator, DrawerItems } from 'react-navigation';

const { width, height } = Dimensions.get("screen");
const widthD = (width * 80) / 100;

const CustomDrawerContentComponent = (props) => (
  <View style={{
    height: height
  }}>
    <ImageBackground
      source={require('./img/MMSAtletaCapa2.jpg')}
      imageStyle={{}}
      style={{
        justifyContent: 'flex-end',
        height: 170,
      }}>
      <View style={{
        flexDirection: 'row',
        marginBottom: 20,
        marginLeft: 20
      }}>
        <View style={{ width: 10, backgroundColor: '#ff6348' }} />
        <Text style={{
          fontWeight: 'bold',
          fontSize: 22,
          width: 150,
          marginLeft: 20,
          color: 'white'
        }}>
          MMS Atleta
            </Text>
      </View>
    </ImageBackground>
    <ScrollView nestedScrollEnabled style={{ marginTop: 10 }}>

      <DrawerItems
        labelStyle={{
          fontSize: 18,
          color: '#001e46',
          marginLeft: 0,
          fontWeight: 'bold',
        }}

        itemStyle={{
          marginHorizontal: 35,
          borderBottomWidth: props == null ? 0 : 0,
          borderBottomColor: '#001e46'
        }} {...props} />
    </ScrollView>
  </View>
);

const StackNavigator = createDrawerNavigator({
  Perfil: {
    screen: Perfil,
    navigationOptions: {
      drawerLabel: 'Perfil',
    }
  },

  LerAntes: {
    screen: LerAntes,
    navigationOptions: {
      drawerLabel: 'Responda antes do treino',
    }
  },

  Planilha: {
    screen: Planilha,
    navigationOptions: {
      drawerLabel: 'Planilha de treino',
    }
  },

  LerDepois: {
    screen: LerDepois,
    navigationOptions: {
      drawerLabel: 'Responda após o treino',
    }
  },

  Calendario: {
    screen: Calendario,
    navigationOptions: {
      drawerLabel: 'Calendário',
    }
  },

  Teste: {
    screen: Teste,
    navigationOptions: {
      drawerLabel: 'Testes',
    }
  },

  ex: {
    screen: Principal,
    navigationOptions: {
      drawerLabel: 'Aquecimentos',
    }
  },

  Financeiro: {
    screen: Financa,
    navigationOptions: {
      drawerLabel: 'Financeiro',
    }
  },

  Exercicio: {
    screen: Exercicio,
    navigationOptions: {
      drawerLabel: 'Exercícios',
    }
  },

  Planos: {
    screen: Planos,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  CartaoCredito: {
    screen: CartaoCredito,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  Login: {
    screen: Login,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  TrocaSenha: {
    screen: TrocaSenha,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  Cadastro: {
    screen: Cadastro,
    navigationOptions: {
      drawerLabel: () => null,
    }
  },

  Aquecimento: {
    screen: Aquecimento,
    navigationOptions: {
      drawerLabel: () => null,
    }
  }
},
  {
    initialRouteName: 'Login',
    drawerLockMode: 'unlocked',
    drawerPosition: 'left',
    contentComponent: CustomDrawerContentComponent,
    drawerBackgroundColor: 'rgba(255,255,255,0.9)',
    drawerWidth: widthD
  });

export default { StackNavigator };