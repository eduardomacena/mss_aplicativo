import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  ImageBackground,
  Picker,
  Platform,
  BackHandler,
  Dimensions,
  Modal,
  ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'
import { vencido } from '../utils/consultaPlano'
import VideoPlayer from 'react-native-video-player';

class Exercicio extends Component {

  constructor() {
    super();

    this.state = {
      orientation: '',
      video: {
        width: undefined,
        height: undefined,
        duration: undefined
      },
      thumbnailUrl: undefined,
      videoUrl: undefined,
      descricao: '',
      emailS: '',
      email: '',
      senha: '',
      idUsuario: '',
      categorias: [],
      exercicios: [],
      selectCategoria: "",
      selectExercicios: "",
      modalVisibleSenha: false,
      viewSelectCategoria: false
    }
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('ex')
      return true;
    })

    const RNFS = require('react-native-fs');
    const path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {

          this.setState({
            idUsuario: String(JSON.parse(contents)[0].idUsuario),
          })


          this.setState({
            refresh: true
          })

          fetch(urlAPI + '/planos/getativos.php?atleta=' + JSON.parse(contents)[0].idUsuario, {}
          ).then((response) => response.json())
            .then((responseJson) => {
              if (!responseJson.message) {
                this.buscaCategorias()
              } else {
                var RNFS = require('react-native-fs');
                var path = RNFS.DocumentDirectoryPath + '/financa.json';

                RNFS.readFile(path, 'utf8')
                  .then((contents) => {
                    if (!(contents == '[{}]') & !(contents == '')) {
                      alert('Seu plano expirou, renove seu plano!')
                      this.props.navigation.navigate('Financeiro')
                      return
                    } else {
                      alert('Seu plano expirou, renove seu plano!')
                      this.props.navigation.navigate('Financeiro')
                      return
                    }
                  })
                  .catch((err) => {
                    alert(err.message)
                  });
              }
            })
            .catch((error) => {
              alert('ocorreu um erro ' + error);
            });

          this.getOrientation();

          Dimensions.addEventListener('change', () => {
            this.getOrientation();
          });

        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );

          this.props.navigation.navigate('ex')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });

  }

  buscaCategorias = async () => {
    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(urlAPI + '/exercicios/get.php', {}
      ).then((response) => response.json())
        .then((responseJson) => {

          if (!responseJson.message) {

            this.setState({
              categorias: responseJson.records,
              selectCategoria: responseJson.records[0].categoria
            })

            this.buscaExercicios()

          } else {
            alert(JSON.stringify(responseJson))
            this.setState({
              refresh: false
            })
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }

  }

  buscaExercicios = async () => {
    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(urlAPI + '/exercicios/get_exercicio.php?categorias=' + this.state.selectCategoria, {}
      ).then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            refresh: false
          })

          if (!responseJson.message) {

            this.setState({
              exercicios: responseJson.records.sort((a, b) => {
                return (a["exercicio"] > b["exercicio"]) ? 1 : ((a["exercicio"] < b["exercicio"]) ? -1 : 0);
              })
            })

          } else {
            alert(JSON.stringify(responseJson))
            this.setState({
              refresh: false
            })
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }

  }

  buscaVideo = async (nVideo) => {

    this.setState({
      video: { width: undefined, height: undefined, duration: undefined },
      thumbnailUrl: undefined,
      videoUrl: undefined
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {

      global.fetch(`https://player.vimeo.com/video/${nVideo}/config`)
        .then(res => res.json())
        .then(res => this.setState({
          thumbnailUrl: res.video.thumbs['640'],
          videoUrl: res.request.files.hls.cdns[res.request.files.hls.default_cdn].url,
          video: res.video,
        }));

    } else {
      alert('Sem conexão com a internet!')
    }
  }

  getOrientation = () => {
    if (Dimensions.get('window').width < Dimensions.get('window').height) {
      this.setState({ orientation: 'portrait' });
    }
    else {
      this.setState({ orientation: 'landscape' });
    }
  }

  /* tela principal */
  render() {

    return (
      this.state.refresh ?
        <ActivityIndicator
          size="large"
          color="white"
          style={{
            flex: 1,
            backgroundColor: '#141b29'
          }}
        /> :

        (this.state.orientation == 'portrait') ?

          <View style={{ backgroundColor: '#141b29', flex: 1 }}>
            <StatusBar
              barStyle='light-content'
              backgroundColor='#1b263a' />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                height: Platform.OS == "android" ? 60 : 90,
                alignItems: 'flex-end',
                backgroundColor: '#1b263a'
              }}>
              <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
                <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
              </TouchableOpacity>

              <Text style={{
                width: '33.33%',
                fontWeight: 'bold',
                fontSize: 16,
                color: 'white',
                marginBottom: Platform.OS == "android" ? 8 : 13,
                marginLeft: 20
              }}>MMS ATLETA</Text>

              <View style={{ width: '33.33%' }} />
            </View>

            <ScrollView nestedScrollEnabled style={{ height: '100%' }}>

              <View style={{
                borderLeftWidth: 4,
                borderColor: '#14b53a',
                marginLeft: 20,
                marginTop: 30,
                marginBottom: 5
              }}>
                <Text style={{
                  color: 'white',
                  fontSize: 10,
                  fontWeight: 'bold',
                  marginLeft: 10
                }}>SELECIONE A CATEGORIA</Text>
              </View>

              <View style={[{

                marginHorizontal: 15,
                paddingHorizontal: 15,
                backgroundColor: 'white',
                borderRadius: 10,
                flexDirection: (Platform.OS == "ios") ? null : 'row',
                alignItems: 'center'
              }, (Platform.OS == "ios") ? {} : { height: 50 }]}>
                {!(Platform.OS == "ios") ? <Picker
                  selectedValue={this.state.selectCategoria}
                  style={{ width: '100%', color: '#ff6348', fontWeight: 'bold', }}
                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({ selectCategoria: itemValue })
                    this.buscaExercicios()
                  }
                  }>
                  {this.state.categorias.map((item, index) => {
                    return (
                      <Picker.Item
                        label={String(item.categoria).toUpperCase()}
                        value={item.categoria}
                        key={index}
                      />
                    );
                  })}
                </Picker> : <View style={{ width: '100%' }}>

                    {<TouchableOpacity
                      style={{
                        justifyContent: "center",
                        height: 50,
                      }}
                      onPress={() => {
                        this.setState({ viewSelectCategoria: true })
                      }}>
                      <Text style={{
                        textAlign: "center"
                      }}>{String(this.state.selectCategoria).toUpperCase()}</Text>
                    </TouchableOpacity>}
                  </View>}

                {!this.state.viewSelectCategoria ? null : <View style={{
                  width: '100%'
                }}>
                  {this.state.categorias.map((item, index) => {
                    return (
                      <TouchableOpacity
                        style={{
                          justifyContent: "center",
                          height: 25,
                        }}
                        onPress={() => {
                          this.setState({
                            selectCategoria: item.categoria,
                            viewSelectCategoria: false
                          })
                          this.buscaExercicios()
                        }}>
                        <Text style={{
                          textAlign: "center",
                          color: 'silver'
                        }}>{String(item.categoria).toUpperCase()}</Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>}
                {(Platform.OS == "ios") ? null : <View style={{
                  backgroundColor: 'white',
                  marginLeft: -35,
                  borderRadius: 18
                }}>
                  <Icon name="downcircle" color='#ff6348' style={{}} size={18} />
                </View>}
              </View>


              <View style={{
                borderLeftWidth: 4,
                borderColor: '#14b53a',
                marginLeft: 20,
                marginTop: 20,
                marginBottom: 5
              }}>
                <Text style={{
                  color: 'white',
                  fontSize: 10,
                  fontWeight: 'bold',
                  marginLeft: 10
                }}>SELECIONE TIPO EXERCÍCIO</Text>
              </View>

              <View style={[{
                marginHorizontal: 15,
                paddingHorizontal: 15,
                backgroundColor: 'white',
                borderRadius: 10,
                flexDirection: (Platform.OS == "ios") ? null : 'row',
                alignItems: 'center'
              }, (Platform.OS == "ios") ? {} : { height: 50 }]}>
                {!(Platform.OS === "ios") ? <Picker
                  selectedValue={this.state.selectExercicios}
                  style={{
                    width: '100%',
                    color: '#ff6348',
                    fontWeight: 'bold',
                    fontSize: '18'
                  }}
                  itemStyle={{
                    fontSize: 10,
                  }}

                  onValueChange={(itemValue, itemIndex) => {
                    this.setState({ selectExercicios: itemValue })
                    this.buscaVideo(itemValue)
                  }}>
                  {this.state.exercicios.map((item, index) => {
                    return (
                      <Picker.Item
                        label={String(item.exercicio).toUpperCase()}
                        value={item.ngif}
                        key={index}
                      />
                    );
                  })}
                </Picker> : <View style={{ width: '100%' }}>

                    {!this.state.viewSelectCategoria ? <TouchableOpacity
                      style={{
                        justifyContent: "center",
                        height: 50,
                      }}
                      onPress={() => {
                        this.setState({ viewSelectExercicio: true })
                      }}>
                      <Text style={{
                        textAlign: "center"
                      }}>{String(this.state.selectExercicios).toUpperCase()}</Text>
                    </TouchableOpacity> : null}
                  </View>}

                {!this.state.viewSelectExercicio ? null : <View style={{
                  width: '100%',
                  height: 200
                }}>
                  <ScrollView nestedScrollEnabled>
                    {this.state.exercicios.map((item, index) => {
                      return (
                        <TouchableOpacity
                          style={{
                            justifyContent: "center",
                            height: 25,
                          }}
                          onPress={() => {
                            this.setState({
                              selectExercicios: item.exercicio,
                              viewSelectExercicio: false
                            })
                            this.buscaVideo(item.ngif)
                          }}>
                          <Text style={{
                            textAlign: "center",
                            color: 'silver'
                          }}>{String(item.exercicio).toUpperCase()}</Text>
                        </TouchableOpacity>
                      );
                    })}
                  </ScrollView>
                </View>}
                {(Platform.OS == "ios") ? null : <View style={{
                  backgroundColor: 'white',
                  marginLeft: -35,
                  borderRadius: 18
                }}>
                  <Icon name="downcircle" color='#ff6348' style={{}} size={18} />
                </View>}
              </View>

              <View style={{
                marginHorizontal: 15,
                justifyContent: 'flex-end',
                backgroundColor: 'white',
                borderRadius: 10,
                marginTop: 20
              }}>
                {/*<Image
                style={{
                  flex: 1,
                  resizeMode: 'stretch'
                }}
                source={{ uri: 'http://mmssystem.com.br/atleta/Videos/' + this.state.selectExercicios + '.gif' }}
              />*/}

                <VideoPlayer
                  endWithThumbnail
                  thumbnail={{ uri: this.state.thumbnailUrl }}
                  video={{ uri: this.state.videoUrl }}
                  videoWidth={this.state.video.width}
                  videoHeight={this.state.video.height}
                  duration={this.state.video.duration/* I'm using a hls stream here, react-native-video
                               can't figure out the length, so I pass it here from the vimeo config */}
                  ref={r => this.player = r}
                />
              </View>
            </ScrollView>
          </View>
          :
          <VideoPlayer
            endWithThumbnail
            thumbnail={{ uri: this.state.thumbnailUrl }}
            video={{ uri: this.state.videoUrl }}
            videoWidth={this.state.video.width}
            videoHeight={this.state.video.height}
            duration={this.state.video.duration/* I'm using a hls stream here, react-native-video
                               can't figure out the length, so I pass it here from the vimeo config */}
            ref={r => this.player = r}
          />
    )
  }
}

export default Exercicio;