import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  ImageBackground,
  Picker,
  Platform,
  BackHandler,
  Modal,
  Dimensions
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API';
import { vencido } from '../utils/consultaPlano';

class Planilha extends Component {
  state = {
    tempo: '',
    pace: '',
    distancia: 0,
    porcentagem: 0,
    dia: '',
    id: 0,
    datatestes: [],
    mode: 'A',
    modalCalculos: false,
    idMes: [
      "Segunda", "Terca", "Quarta",
      "Quinta", "Sexta", "Sabado",
      "Domingo"
    ],
    idMesInicio: [
      "Domingo", "Segunda", "Terca", "Quarta",
      "Quinta", "Sexta", "Sabado"
    ],
    diaAtual: new Date(),
    dia: '',
    modalVisibleSenha: false,
    datasourse: []
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('ex')
      return true;
    })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then(async (contents) => {
        if (!(contents == '[{}]')) {
          if (vencido(String(JSON.parse(contents)[0].idUsuario))) {

            alert('Seu plano expirou, renove seu plano!')

            this.props.navigation.navigate('Financeiro')
          } else {

            this.setState({
              idUsuario: String(JSON.parse(contents)[0].idUsuario),
              idDesk: String(JSON.parse(contents)[0].idDesk),
              tecnico: String(JSON.parse(contents)[0].tecnico)
            })

            this.buscaPlano()

            this.setState({
              refresh: true
            })

            const isConnected = await checkInternetConnection();
            if (isConnected) {
              var data = new Date();
              let dia = data.getDay();


              fetch(urlAPI + '/teste/get.php?tecnico=' + String(JSON.parse(contents)[0].tecnico) + '&atleta=' + String(JSON.parse(contents)[0].idDesk), {}
              ).then((response) => response.json())
                .then((responseJson) => {

                  if (!responseJson.message) {
                    this.setState({
                      datatestes: responseJson.records,
                    })

                  } else {
                    alert('Nenhum evento encontrado')
                    this.setState({
                      refresh: false
                    })
                  }
                })
                .catch((error) => {
                  alert('ocorreu um erro ' + error);
                  this.setState({
                    refresh: false
                  })
                });

              fetch(urlAPI + '/microciclo/get.php?tecnico=' + String(JSON.parse(contents)[0].tecnico) +
                '&atleta=' + String(JSON.parse(contents)[0].idDesk) +
                '&mode=' + this.state.mode +
                '&dia=' + this.state.idMesInicio[dia], {}
              ).then((response) => response.json())
                .then((responseJson) => {

                  this.setState({
                    refresh: false
                  })

                  if (!responseJson.message) {

                    this.setState({
                      datasourse: responseJson.records
                    })

                    const idDia = (this.state.idMesInicio[dia] == "Segunda" ? 0 :
                      this.state.idMesInicio[dia] == "Terca" ? 1 :
                        this.state.idMesInicio[dia] == "Quarta" ? 2 :
                          this.state.idMesInicio[dia] == "Quinta" ? 3 :
                            this.state.idMesInicio[dia] == "Sexta" ? 4 :
                              this.state.idMesInicio[dia] == "Sabado" ? 5 :
                                this.state.idMesInicio[dia] == "Domingo" ? 6 : 0);

                    const element = this.state.datasourse[0];

                    let diaz = element[this.state.idMes[idDia]];

                    this.setState({
                      dia: diaz,
                      id: idDia
                    });

                  } else {
                    alert(JSON.stringify(responseJson))
                    this.setState({
                      refresh: false
                    })
                  }
                })
                .catch((error) => {
                  alert('ocorreu um erro ' + error);
                  this.setState({
                    refresh: false
                  })
                });

            } else {
              alert('Sem conexão com a internet!')
              this.setState({
                refresh: false
              })
            }
          }
        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );

          this.props.navigation.navigate('ex')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });

  }

  avancaDia = async () => {
    this.setState({
      refresh: true,
      id: (this.state.id + 1)
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {

      fetch(urlAPI + '/microciclo/get.php?tecnico=' + this.state.tecnico +
        '&atleta=' + this.state.idDesk +
        '&mode=' + this.state.mode +
        '&dia=' + this.state.idMes[this.state.id], {}
      ).then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            refresh: false
          })
          if (!responseJson.message) {


            this.setState({
              datasourse: responseJson.records
            })

            const element = this.state.datasourse[0];
            let dia = element[this.state.idMes[this.state.id]];

            this.setState({
              dia: dia
            })

          } else {
            alert('Nenhum evento encontrado')
            this.setState({
              refresh: false
            })
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  retroDia = async () => {
    this.setState({
      refresh: true,
      id: (this.state.id - 1) < 0 ? 0 : (this.state.id - 1)
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {

      fetch(urlAPI + '/microciclo/get.php?tecnico=' + this.state.tecnico +
        '&atleta=' + this.state.idDesk +
        '&mode=' + this.state.mode +
        '&dia=' + this.state.idMes[this.state.id], {}
      ).then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            refresh: false
          })

          if (!responseJson.message) {

            this.setState({
              datasourse: responseJson.records
            })

            const element = this.state.datasourse[0];
            let dia = element[this.state.idMes[this.state.id]];

            this.setState({
              dia: dia
            })

          } else {
            alert('Nenhum evento encontrado')
            this.setState({
              refresh: false
            })
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  diaHoje = () => {
    var date = new Date()

    var monthNames = [
      "JANEIRO", "FEVEREIRO", "MARÇO",
      "ABRIL", "MAIO", "JUNHO", "JULHO",
      "AGOSTO", "SETEMBRO", "OUTUBRO",
      "NOVEMBRO", "DEZEMBRO"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return String(day + ' ' + monthNames[monthIndex] + ' ' + year);

  }

  criaTreino = () => {
    let treinos = []
    let lines = []

    for (let index = 0; index < this.state.datasourse.length; index++) {
      const element = this.state.datasourse[index];

      lines.push(
        <Text style={{
          fontSize: 14,
          color: 'black',
          marginHorizontal: 10,
          marginVertical: 10
        }}>{element[this.state.idMes[this.state.id]]}</Text>
      )
    }

    treinos.push(
      <View style={{
        height: (18 * this.state.datasourse.length),
        borderRadius: 10,
        backgroundColor: 'white',
        marginHorizontal: 20,
        marginVertical: 5,
      }}>
        <View style={{
          height: 20,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          justifyContent: 'center',
          backgroundColor: '#2ed573'
        }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 14,
              color: 'white'
            }}>TREINO</Text>
        </View>


        <Text style={{
          fontSize: 13,
          fontWeight: 'bold',
          marginHorizontal: 10,
          marginTop: 5
        }}>TREINO</Text>
        <ScrollView
          ref='_scrollView'
          onContentSizeChange={() => { this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true }); }}
        >
          {lines}
        </ScrollView>
      </View>
    )

    return treinos
  }

  buscaPlano = async () => {
    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(urlAPI + '/planos/getativos.php?atleta=' + this.state.idUsuario, {}
      ).then((response) => response.json())
        .then((responseJson) => {
          if (!responseJson.message) {

          } else {
            alert('Plano expirado, ative seu plano.')
            this.props.navigation.navigate('Financeiro')
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });
    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  somaVcrit = () => {

    String.prototype.toHHMMSS = function () {
      var sec_num = parseInt(this, 10); // don't forget the second param
      var hours = Math.floor(sec_num / 3600);
      var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
      var seconds = sec_num - (hours * 3600) - (minutes * 60);

      if (hours < 10) { hours = "0" + hours; }
      if (minutes < 10) { minutes = "0" + minutes; }
      if (seconds < 10) { seconds = "0" + seconds; }
      return hours + ':' + minutes + ':' + seconds;
    }

    let vcrit = this.state.datatestes[15].resultado

    let aux = ((this.state.porcentagem * vcrit) / 100)

    let tempos = parseFloat(this.state.distancia / aux).toFixed(0)
    let decSec = String(parseFloat(this.state.distancia / aux).toFixed(3)).substr(-3)

    let paceTempos = parseFloat(1000 / aux).toFixed(0);
    let paceDecSec = String(parseFloat(1000 / aux).toFixed(3)).substr(-3);


    this.setState({
      tempo: (String(tempos).toHHMMSS() + '.' + decSec),
      pace: (String(paceTempos).toHHMMSS() + '.' + paceDecSec)
    })
  }

  somatmax = () => {

    String.prototype.toHHMMSS = function () {
      var sec_num = parseInt(this, 10); // don't forget the second param
      var hours = Math.floor(sec_num / 3600);
      var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
      var seconds = sec_num - (hours * 3600) - (minutes * 60);

      if (hours < 10) { hours = "0" + hours; }
      if (minutes < 10) { minutes = "0" + minutes; }
      if (seconds < 10) { seconds = "0" + seconds; }
      return hours + ':' + minutes + ':' + seconds;
    }

    let tmax = this.searchFilterDistace(String(this.state.distancia))

    let aux = (2 * tmax) - ((tmax * this.state.porcentagem) / 100)


    let tempos = String(parseFloat(aux).toFixed(1)).split(".")[0];

    let decSec = String(parseFloat(aux).toFixed(3)).substr(-3)

    this.setState({
      tempo: (String(tempos).toHHMMSS() + '.' + decSec),
      pace: '-'
    })

  }

  /*BUSCA POR DISTANCIA*/
  searchFilterDistace = (text) => {
    const newData = this.state.datatestes.filter(item => {
      const itemData = `${item.teste.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    const result = newData[0].resultado

    return result
  };


  /* tela principal */
  render() {
    const element = this.state.datasourse[0];
    let { height, width } = Dimensions.get("screen")
    return (
      <View style={{ backgroundColor: '#141b29', flex: 1 }}>
        <StatusBar
          barStyle='light-content'
          backgroundColor='#1b263a' />

        <Modal
          visible={this.state.modalCalculos}
          onRequestClose={() => { this.setState({ modalCalculos: false }) }}>
          <View style={{
            backgroundColor: '#1b263a',
            justifyContent: 'space-between',
            flex: 1
          }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({ modalCalculos: false })
              }}
              style={{
                height: 35,
                width: 35,
                marginLeft: 20,
                marginTop: 20,
                backgroundColor: '#ff6348',
                borderRadius: 35,
                justifyContent: 'center'
              }}>
              <Icon
                name='arrowleft'
                size={20}
                style={{
                  alignSelf: 'center'
                }}
              />
            </TouchableOpacity>

            <View>
              <Text style={styles.texteCalculo}>DISTÂNCIA</Text>
              <TextInput
                value={this.state.distancia}
                onChangeText={(distancia) => { this.setState({ distancia }) }}
                placeholder='0'
                style={styles.editsCauculo}
                keyboardType='numeric'
                returnKeyLabel='Done'
                returnKeyType='done'
                underlineColorAndroid={'transparent'} />


              <Text style={styles.texteCalculo}>PORCENT. (%)</Text>
              <TextInput
                value={this.state.porcentagem}
                onChangeText={(porcentagem) => { this.setState({ porcentagem }) }}
                placeholder='0'
                style={styles.editsCauculo}
                keyboardType='numeric'
                returnKeyLabel='Done'
                returnKeyType='done'
                underlineColorAndroid={'transparent'} />
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginVertical: 20
              }}>
              <TouchableOpacity
                onPress={() => { this.somaVcrit() }}
                style={{
                  height: 45,
                  width: '40%',
                  borderRadius: 10,
                  marginHorizontal: 5,
                  justifyContent: 'center',
                  backgroundColor: '#ff9f43'
                }}>
                <Text style={styles.textButtom}>VCRIT</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => { this.somatmax() }}
                style={{
                  height: 45,
                  width: '40%',
                  borderRadius: 10,
                  justifyContent: 'center',
                  backgroundColor: '#1dd1a1'
                }}>
                <Text style={styles.textButtom}>TMÁX</Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                marginVertical: 20
              }}>
              <Text style={styles.texteCalculo}>TEMPO</Text>
              <TextInput
                editable={false}
                value={this.state.tempo}
                placeholder='00:00:00.000'
                style={[styles.editsCauculo, {fontWeight: 'bold'}]}
                keyboardType='numeric'
                returnKeyLabel='Done'
                returnKeyType='done'
                underlineColorAndroid={'transparent'} />

              <Text style={styles.texteCalculo}>PACE</Text>
              <TextInput
                editable={false}
                value={this.state.pace}
                placeholder='00:00:00.000'
                style={[styles.editsCauculo, {fontWeight: 'bold'}]}
                keyboardType='numeric'
                returnKeyLabel='Done'
                returnKeyType='done'
                underlineColorAndroid={'transparent'} />
            </View>

            <View />
          </View>
        </Modal>


        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
          height: Platform.OS == "android" ? 60 : 90,
          alignItems: 'flex-end',
          backgroundColor: '#1b263a'
        }}>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
          </TouchableOpacity>

          <Text style={{
            width: '33.33%',
            fontWeight: 'bold',
            fontSize: 16,
            color: 'white',
            marginBottom: Platform.OS == "android" ? 8 : 13,
            marginLeft: 20
          }}>MMS ATLETA</Text>

          <View style={{ width: '33.33%' }} />
        </View>

        <View style={{ flex: 1, justifyContent: 'space-between' }}>
          {this.state.datasourse ?
            <View
              style={{
                flexDirection: 'row',
                marginTop: 20,
                alignItems: 'center',
                justifyContent: 'space-between',
                marginHorizontal: 20
              }}>
              <TouchableOpacity
                onPress={() => { this.retroDia() }}
                style={{
                  height: 40,
                  width: 40,
                  justifyContent: 'center',
                  borderRadius: 40,
                  backgroundColor: '#ff793f'
                }}>
                <Icon name='arrowleft' size={20} color='#1b263a' style={{ alignSelf: 'center' }} />
              </TouchableOpacity>

              <Text style={{ color: 'white', }}>
                {this.state.dia}
              </Text>

              <TouchableOpacity
                onPress={() => { this.avancaDia() }}
                style={{
                  height: 40,
                  width: 40,
                  justifyContent: 'center',
                  borderRadius: 40,
                  backgroundColor: '#ff793f'
                }}>
                <Icon name='arrowright' size={20} color='#1b263a' style={{ alignSelf: 'center' }} />
              </TouchableOpacity>
            </View>
            : null
          }
          <ScrollView
            style={{
              flex: 1,
              marginTop: 20
            }}>
            {this.criaTreino()}
          </ScrollView>

          <View
            style={{
              position: 'absolute',
              height: '10%',
              width: '100%',
              paddingRight: 20,
              paddingBottom: 20,
              marginTop: (height * 75) / 100,
              alignItems: 'flex-end',
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
            <View />

            <TouchableOpacity
              onPress={() => {
                this.setState({ modalCalculos: true })
              }}
              style={{
                height: 60,
                width: 60,
                backgroundColor: '#7bed9f',
                borderRadius: 60,
                justifyContent: 'center'
              }}>
              <Icon
                name='calculator'
                size={28}
                style={{
                  alignSelf: 'center'
                }}
              />
            </TouchableOpacity>
          </View>

        </View>

      </View>
    )
  }
}

export default Planilha;

const styles = StyleSheet.create({
  editsCauculo: {
    textAlign: 'center',
    backgroundColor: 'white',
    color: 'black',
    borderRadius: 10,
    marginHorizontal: '10%'
  },
  texteCalculo: {
    color: 'white',
    fontSize: 10,
    marginVertical: 5,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  textButtom: {
    textAlign: 'center',
    fontSize: 13,
    color: 'white',
    fontWeight: 'bold'
  }
});