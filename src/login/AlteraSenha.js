import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Button,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Image,
    Alert,
    ImageBackground,
    Picker,
    Platform,
    BackHandler,
    ActivityIndicator,
    Modal,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'

class AlteraSenha extends Component {
    state = {
        colors: [
            '#2ed573',
        ]
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ex')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {
                    this.setState({
                        idUsuario: String(JSON.parse(contents)[0].idUsuario),
                        senha: String(JSON.parse(contents)[0].senha)
                    })


                } else {
                    Alert.alert(
                        'Confirmação',
                        'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                        [
                            { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                            { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                        ]
                    );

                    this.props.navigation.navigate('ex')
                    this.setState({ idUsuario: '0' })
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            });

    }

    trocaSenha = async () => {

        if (this.state.senhaAtual == '' | !this.state.senhaAtual |
            this.state.senhaNova == '' | !this.state.senhaNova |
            this.state.senhaConfirma == '' | !this.state.senhaConfirma) {

            alert('Preencha todos os campo!')
            return
        }

        if (!(this.state.senhaAtual == this.state.senha)) {
            alert('Senha atual está incorreta!')
            return
        }

        if (!(this.state.senhaNova == this.state.senhaConfirma)) {
            alert('Os campos "Nova Senha" e "Confirma Nova Senha" não estão iguais!')
            return
        }

        const isConnected = await checkInternetConnection();
        if (isConnected) {

            fetch(urlAPI + '/atletas/altera_senha.php?senha=' + this.state.senhaNova + '&atleta=' + this.state.idUsuario, {}
            ).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.message == 'senha alterada com sucesso') {
                        alert('Senha alterada com sucesso, para concluir o aplicativo abrirá a tela de login.')
                        this.props.navigation.navigate('Login')
                    }else{
                        alert('Sua troca de senha não foi efetuada, entre em contato com o suporte.')
                    }
                })
                .catch((error) => {
                    alert('ocorreu um erro ' + JSON.stringify(error));
                    this.setState({
                        refresh: false
                    })
                });
        } else {
            alert('Sem conexão com a internet!')
            this.setState({
                refresh: false
            })
        }
    }

    /* tela principal */
    render() {
        return (
            this.state.refresh ? <ActivityIndicator style={{ flex: 1, backgroundColor: '#1b263a' }} color="white" size="large" /> :
                <View style={{ backgroundColor: '#141b29', flex: 1 }}>
                    <StatusBar
                        barStyle='light-content'
                        backgroundColor='#1b263a' />

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                        height: Platform.OS == "android" ? 60 : 90,
                        alignItems: 'flex-end',
                        backgroundColor: '#1b263a'
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
                            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
                        </TouchableOpacity>

                        <Text style={{
                            width: '33.33%',
                            fontWeight: 'bold',
                            fontSize: 16,
                            color: 'white',
                            marginBottom: Platform.OS == "android" ? 8 : 13,
                            marginLeft: 20
                        }}>MMS ATLETA</Text>

                        <View style={{ width: '33.33%' }} />
                    </View>

                    <View style={styles.camposEdit}>
                        <TextInput
                            placeholder='Senha Atual'
                            onChangeText={(senhaAtual) => { this.setState({ senhaAtual }) }}
                            secureTextEntry={true}
                            style={{
                                textAlign: "center"
                            }}
                        />
                    </View>

                    <View style={styles.camposEdit}>
                        <TextInput
                            placeholder='Nova Senha'
                            onChangeText={(senhaNova) => { this.setState({ senhaNova }) }}
                            secureTextEntry={true}
                            style={{
                                textAlign: "center"
                            }}
                        />
                    </View>

                    <View style={styles.camposEdit}>
                        <TextInput
                            placeholder='Confirma Nova senha'
                            onChangeText={(senhaConfirma) => { this.setState({ senhaConfirma }) }}
                            secureTextEntry={true}
                            style={{
                                textAlign: "center"
                            }}
                        />
                    </View>

                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginTop: 30,
                            marginHorizontal: 20
                        }}>
                        <TouchableOpacity style={{
                            paddingVertical: 10,
                            paddingHorizontal: 10,
                            justifyContent: "center",
                            borderRadius: 5,
                            backgroundColor: '#eb3b5a'
                        }}>
                            <Text style={styles.textButton}>Cancelar</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                Alert.alert(
                                    'Confirmação',
                                    'Certeza que deseja modificar sua senha ?',
                                    [
                                        { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                        { text: 'SIM', onPress: () => { this.trocaSenha() } },
                                    ]
                                );
                            }}
                            style={{
                                paddingVertical: 10,
                                paddingHorizontal: 10,
                                borderRadius: 5,
                                backgroundColor: '#20bf6b'
                            }}>
                            <Text style={styles.textButton}>Salvar</Text>
                        </TouchableOpacity>
                    </View>

                </View>
        )
    }
}

export default AlteraSenha;

const styles = StyleSheet.create({
    camposEdit: {
        backgroundColor: 'white',
        marginHorizontal: 20,
        height: 40,
        marginTop: 20,
        borderRadius: 40,
        padding: 10,
    },
    textButton: {
        color: 'white',
        fontSize: 16,
    }
})