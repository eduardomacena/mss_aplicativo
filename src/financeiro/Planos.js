import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Button,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Image,
    Alert,
    ImageBackground,
    Picker,
    Platform,
    BackHandler,
    ActivityIndicator,
    Modal,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'

class Planos extends Component {
    state = {
        dia: '',
        diaAtual: new Date(),
        modalVisibleSenha: false,
        planos: [],
        colors: [
            '#2ed573',
        ]
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('Financa')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {
                    this.setState({
                        idUsuario: String(JSON.parse(contents)[0].idUsuario),
                    })

                    this.setState({
                        refresh: true
                    })

                    this.buscaPlano(String(JSON.parse(contents)[0].tecnico))
                } else {
                    Alert.alert(
                        'Confirmação',
                        'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                        [
                            { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                            { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                        ]
                    );

                    this.props.navigation.navigate('ex')
                    this.setState({ idUsuario: '0' })
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                Alert.alert(
                    'Confirmação',
                    'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                    [
                        { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                        { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                    ]
                );

                this.props.navigation.navigate('ex')
                console.log(err.message, err.code);
            });

    }

    buscaPlano = async (id) => {
        const isConnected = await checkInternetConnection();
        if (isConnected) {
            fetch(urlAPI + '/planos/get.php?tecnico=' + id, {}
            ).then((response) => response.json())
                .then((responseJson) => {

                    if (!responseJson.message) {

                        this.setState({
                            planos: responseJson.records,
                            refresh: false
                        })

                    } else {
                        alert(JSON.stringify(responseJson))
                        this.setState({
                            refresh: false,
                            planos: []
                        })
                    }
                })
                .catch((error) => {
                    alert('ocorreu um erro ' + error);
                    this.setState({
                        refresh: false
                    })
                });
        } else {
            alert('Sem conexão com a internet!')
            this.setState({
                refresh: false
            })
        }
    }

    criaPlanos = () => {
        let lista = []
        for (let index = 0; index < this.state.planos.length; index++) {
            let element = this.state.planos[index];

            lista.push(
                <View
                    style={{
                        height: 180,
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        marginHorizontal: 20,
                        marginBottom: 10,
                        borderRadius: 10
                    }}>
                    <View>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginHorizontal: 10,
                                justifyContent: 'space-between'
                            }}>
                            {index !== 1 ? null :
                                <View style={{
                                    height: 40,
                                    width: 25,
                                    justifyContent: 'flex-end',
                                    elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                                    position: 'absolute',
                                    borderBottomLeftRadius: 20,
                                    borderBottomRightRadius: 20,
                                    backgroundColor: '#1b263a'
                                }}>
                                    <Icon name='star' color='white' style={{ alignSelf: 'center', marginBottom: 10 }} />
                                </View>
                            }

                            <View />
                            <View style={{
                                padding: 10,
                                borderBottomLeftRadius: 10,
                                borderBottomRightRadius: 10,
                                backgroundColor: '#FFC312'
                            }}>
                                <Text style={{
                                    fontSize: 10,
                                    textAlign: 'center',
                                    color: 'white'
                                }}>{element.tipo}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        alignSelf: 'center'
                    }}>
                        <Text style={{
                            marginTop: 10,
                            fontSize: 30,
                            fontWeight: "bold",
                            marginRight: 5,
                            color: 'black'
                        }}>R$</Text>
                        <Text style={{
                            fontSize: 60,
                            fontWeight: "bold",
                            color: 'black'
                        }}>{String(String(parseInt(element.parcela)).substr(0, 3)).replace('.', '')},</Text>
                        <View>
                            <Text style={{
                                fontSize: 30,
                                fontWeight: "bold",
                                color: 'black',
                                marginTop: 10
                            }}>{String(String(String(element.parcela).substr(-2)).replace('.', '')).length == 1 ?
                                String(String(element.parcela).substr(-2)).replace('.', '') + "0" :
                                String(String(element.parcela).substr(-2)).replace('.', '')}</Text>
                            <Text style={{
                                fontSize: 10,
                                marginLeft: 5,
                                color: 'black',
                                textAlign: 'center',
                            }}>/Mês</Text>
                        </View>
                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            this.renovarAtivacao(index)
                        }}
                        style={{
                            height: 35,
                            backgroundColor: '#2ecc71',
                            margin: 5,
                            borderRadius: 10,
                            justifyContent: 'center'
                        }}>
                        <Text style={{
                            color: 'white',
                            fontSize: 12,
                            textAlign: 'center'
                        }}>ASSINAR</Text>
                    </TouchableOpacity>
                </View>
            )
        }

        return lista
    }

    renovarAtivacao = (index) => {
        const RNFS = require('react-native-fs');
        const cadastro = RNFS.DocumentDirectoryPath + '/ativaPlano.json';

        let dados = [];

        dados.push({
            "idAtleta": this.state.idUsuario,
            "idPlano": this.state.planos[index].id,
            "tipo": this.state.planos[index].tipo,
            "valor": this.state.planos[index].valor
        })

        RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
            .then((success) => {
                console.log('FILE WRITTEN!');
            })
            .catch((err) => {
                console.log(err.message);
            });

        this.props.navigation.navigate('CartaoCredito')
    }


    /* tela principal */
    render() {
        return (
            <View style={{ backgroundColor: '#141b29', flex: 1 }}>
                <StatusBar
                    barStyle='light-content'
                    backgroundColor='#1b263a' />

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                    height: Platform.OS == "android" ? 60 : 90,
                    alignItems: 'flex-end',
                    backgroundColor: '#1b263a'
                }}>
                    <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
                        <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
                    </TouchableOpacity>

                    <Text style={{
                        width: '33.33%',
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: 'white',
                        marginBottom: Platform.OS == "android" ? 8 : 13,
                        marginLeft: 20
                    }}>MMS ATLETA</Text>

                    <View style={{ width: '33.33%' }} />
                </View>

                {this.state.refresh ? <ActivityIndicator style={{ flex: 1, backgroundColor: '#1b263a' }} color="white" size="large" /> :
                    <ScrollView style={{
                        flex: 1,
                        marginTop: 20
                    }}>
                        {this.criaPlanos()}
                    </ScrollView>}

            </View>
        )
    }
}

export default Planos;