import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Button,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Image,
    Alert,
    ImageBackground,
    Picker,
    Platform,
    BackHandler,
    ActivityIndicator,
    Modal,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'

class Financa extends Component {
    state = {
        dia: '',
        diaAtual: new Date(),
        modalVisibleSenha: false,
        refresh: true,
        planos: [],
        expirado: false,
        colors: [
            '#2ed573',
        ]
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('ex')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/usuario.json';

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]')) {
                    this.setState({
                        idUsuario: String(JSON.parse(contents)[0].idUsuario),
                    })

                    this.buscaPlano()
                } else {
                    Alert.alert(
                        'Confirmação',
                        'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
                        [
                            { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                            { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
                        ]
                    );

                    this.props.navigation.navigate('ex')
                    this.setState({ idUsuario: '0' })
                }
            })
            .catch((err) => {
                this.setState({ idUsuario: '0' })
                alert(err.message)
                console.log(err.message, err.code);
            });

    }

    buscaPlano = async () => {
        this.setState({
            refresh: true
        })

        const isConnected = await checkInternetConnection();
        if (isConnected) {
            fetch(urlAPI + '/planos/getativos.php?atleta=' + this.state.idUsuario, {}
            ).then((response) => response.json())
                .then((responseJson) => {

                    if (!responseJson.message) {
                        this.setState({
                            planos: responseJson.records,
                            refresh: false,
                        })

                        const RNFS = require('react-native-fs');
                        const cadastro = RNFS.DocumentDirectoryPath + '/financa.json';

                        let dados = [];

                        dados.push({
                            "idAtleta": responseJson.records[0].idAtleta,
                            "idPlano": responseJson.records[0].idPlano,
                            "dataAtivacao": responseJson.records[0].dataAtivacao,
                            "tipo": responseJson.records[0].tipo,
                            "parcela": responseJson.records[0].parcela,
                            "qtdParcela": responseJson.records[0].qtdParcela,
                            "valor": responseJson.records[0].valor,
                            "dias": responseJson.records[0].dias
                        })

                        RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
                            .then((success) => {
                                console.log('FILE WRITTEN!');
                            })
                            .catch((err) => {
                                console.log(err.message);
                            });
                    } else {
                        var RNFS = require('react-native-fs');
                        var path = RNFS.DocumentDirectoryPath + '/financa.json';

                        RNFS.readFile(path, 'utf8')
                            .then((contents) => {
                                if (!(contents == '[{}]') & !(contents == '')) {
                                    this.setState({
                                        planos: JSON.parse(contents),
                                        refresh: false,
                                        expirado: true
                                    })
                                } else {
                                    alert('Nenhum plano assinado nessa conta, consulte o suporte')
                                    this.props.navigation.navigate('ex')
                                }
                            })
                            .catch((err) => {
                                alert(err.message)
                                this.props.navigation.navigate('ex')
                            });
                    }
                })
                .catch((error) => {
                    alert('ocorreu um erro ' + error);
                    this.setState({
                        refresh: false
                    })
                });
        } else {
            alert('Sem conexão com a internet!')
            this.setState({
                refresh: false
            })
        }
    }

    renovarAtivacao = () => {
        const RNFS = require('react-native-fs');
        const cadastro = RNFS.DocumentDirectoryPath + '/ativaPlano.json';

        let dados = [];

        dados.push({
            "idAtleta": this.state.planos[0].idAtleta,
            "idPlano": this.state.planos[0].idPlano,
            "tipo": this.state.planos[0].tipo,
            "valor": this.state.planos[0].valor,
            "parcela": this.state.planos[0].parcela,
            "qtdParcela": this.state.planos[0].qtdParcela
        })

        RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
            .then((success) => {
                console.log('FILE WRITTEN!');
            })
            .catch((err) => {
                console.log(err.message);
            });

        this.props.navigation.navigate('CartaoCredito')
    }

    vencimento = () => {

        const dia = new Date(String(this.state.planos[0].dataAtivacao).substr(0, 4),
            (parseInt(String(this.state.planos[0].dataAtivacao).substr(5, 2)) - 1),
            parseInt(String(this.state.planos[0].dataAtivacao).substr(8, 2)), 0, 0, 0, 0
        );

        dia.setDate(dia.getDate() + parseInt(this.state.planos[0].dias))

        const day = dia.getDate()
        const month = String(dia.getMonth() + 1).length > 1 ? (dia.getMonth() + 1) : ("0" + String(dia.getMonth() + 1))
        const year = dia.getFullYear()

        /*return String(String(this.state.planos[0].dataAtivacao).substr(0, 4)+
        String(this.state.planos[0].dataAtivacao).substr(5, 2)+
        String(this.state.planos[0].dataAtivacao).substr(8, 2));*/
        return String(day + "/" + month + "/" + year);
    }

    /* tela principal */
    render() {
        return (
            this.state.refresh ? <ActivityIndicator style={{ flex: 1, backgroundColor: '#1b263a' }} color="white" size="large" /> :
                <View style={{ backgroundColor: '#141b29', flex: 1 }}>
                    <StatusBar
                        barStyle='light-content'
                        backgroundColor='#1b263a' />

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                        height: Platform.OS == "android" ? 60 : 90,
                        alignItems: 'flex-end',
                        backgroundColor: '#1b263a'
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
                            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
                        </TouchableOpacity>

                        <Text style={{
                            width: '33.33%',
                            fontWeight: 'bold',
                            fontSize: 16,
                            color: 'white',
                            marginBottom: Platform.OS == "android" ? 8 : 13,
                            marginLeft: 20
                        }}>MMS ATLETA</Text>

                        <View style={{ width: '33.33%' }} />
                    </View>

                    <ScrollView style={{
                        flex: 1,
                        marginTop: 20
                    }}>
                        <View style={{
                            height: 200,
                            backgroundColor: 'white',
                            justifyContent: 'space-between',
                            marginHorizontal: 20,
                            borderRadius: 10
                        }}>
                            <View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginHorizontal: 10,
                                        justifyContent: 'space-between'
                                    }}>
                                    <View>
                                        <Text style={{
                                            fontSize: 12,
                                            marginRight: 10,
                                            fontWeight: "bold"
                                        }}>{this.state.planos[0].tipo}</Text>
                                    </View>

                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        padding: 10,
                                        borderBottomLeftRadius: 10,
                                        borderBottomRightRadius: 10,
                                        backgroundColor: '#FFC312',
                                        elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                                    }}>
                                        <Text style={{
                                            fontSize: 12,
                                            marginRight: 10,
                                            fontWeight: "bold",
                                            color: '#1b263a'
                                        }}>{this.state.expirado ? "EXPIRADO" : "ATIVO"}</Text>
                                        <View style={{
                                            height: 10,
                                            width: 10,
                                            borderRadius: 10,
                                            elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                                            backgroundColor: this.state.expirado ? '#e74c3c' : '#2ecc71'
                                        }} />
                                    </View>
                                </View>
                            </View>

                            <View style={{
                                flexDirection: 'row',
                                alignSelf: 'center'
                            }}>
                                <Text style={{
                                    color: 'black',
                                    marginTop: 10,
                                    fontSize: 30,
                                    fontWeight: "bold",
                                    marginRight: 10
                                }}>R$</Text>
                                <Text style={{
                                    fontSize: 60,
                                    fontWeight: "bold",
                                    color: 'black'
                                }}>{String(parseFloat(this.state.planos[0].parcela).toFixed(2)).length == 5 ?
                                    String(parseFloat(this.state.planos[0].parcela).toFixed(2)).substr(0, 2) :
                                    String(parseFloat(this.state.planos[0].parcela).toFixed(2)).length == 4 ?
                                        String(parseFloat(this.state.planos[0].parcela).toFixed(2)).substr(0, 1) :
                                        String(parseFloat(this.state.planos[0].parcela).toFixed(2)).substr(0, 3)},</Text>
                                <Text style={{
                                    color: 'black',
                                    fontSize: 30,
                                    fontWeight: "bold",
                                    marginTop: 10
                                }}>{String(parseFloat(this.state.planos[0].valor).toFixed(2)).substr(-2)}</Text>
                            </View>

                            <Text style={{
                                color: 'red',
                                fontSize: 12,
                                textAlign: 'center',
                                marginBottom: -12
                            }}>Vencimento {this.vencimento()}</Text>

                            <TouchableOpacity
                                onPress={() => { this.renovarAtivacao() }}
                                style={{
                                    height: 35,
                                    backgroundColor: '#1b263a',
                                    margin: 5,
                                    elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                                    borderRadius: 10,
                                    justifyContent: 'center'
                                }}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 12,
                                    textAlign: 'center'
                                }}>RENOVAR</Text>
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('Planos') }}
                            style={{
                                height: 35,
                                backgroundColor: '#2ecc71',
                                margin: 5,
                                borderRadius: 10,
                                marginHorizontal: 20,
                                justifyContent: 'center'
                            }}>
                            <Text style={{
                                color: 'white',
                                fontSize: 12,
                                textAlign: 'center'
                            }}>ADQUIRIR OUTRO PLANO</Text>
                        </TouchableOpacity>
                    </ScrollView>

                </View>
        )
    }
}

export default Financa;