import React, { Component } from "react";
import {
    View,
    ScrollView,
    Text,
    StyleSheet,
    Alert,
    Image,
    TouchableOpacity,
    AsyncStorage,
    StatusBar,
} from "react-native";
import MaterialsIcon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/AntDesign';
import { Kohana } from 'react-native-textinput-effects';
import { Snackbar, Portal, Provider as PaperProvider } from 'react-native-paper';
import Confetti from 'react-native-confetti';

class FimTriste extends React.Component {
    state = {
        nomeUsr: '',
        preco: '',
        visible: false,
        cont: 0,
        dataSource: [],
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        if (this._confettiView) {
            this._confettiView.startConfetti();
        }
    }

    render() {
        const { visible } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#ff793f', justifyContent: 'center', alignContent: 'center' }}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#ff793f'
                />

                <Text style={[styles.textPag, { fontSize: 60, fontWeight: 'bold' }]}>Que triste...</Text>
                <Text style={styles.textPag}>Operação negada!</Text>
                <Text style={styles.textPag}>Ocorreu algum problema, com</Text>
                <Text style={styles.textPag}>as suas informações, tente novamente</Text>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('AssinaPlano')} style={{
                    marginTop: 60,
                    marginRight: '25%',
                    marginLeft: '25%',
                    borderRadius: 50,
                    height: 50,
                    justifyContent: 'center',
                    elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                    alignContent: 'center',
                    backgroundColor: '#40407a',
                }}>
                    <Text style={{
                        fontWeight: 'bold',
                        fontSize: 14,
                        color: '#ff793f',
                        justifyContent: 'center',
                        alignSelf: 'center',
                        alignContent: 'center',
                    }}>OK</Text>
                </TouchableOpacity>
                
            </View>
        );
    }
}

export default FimTriste;
const styles = StyleSheet.create({
    textPag: {
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        fontSize: 14,
        marginBottom: 1,
        color: 'white',
        fontFamily: 'notoserif',
    },
    rodape: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
        marginBottom: 20,
        marginRight: 20,
        borderRadius: 60,
        backgroundColor: '#26de81',
        width: 60,
        height: 60,
    },
    rodape2: {
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
        marginBottom: 20,
        marginLeft: 20,
        borderRadius: 60,
        backgroundColor: '#ff793f',
        width: 60,
        height: 60,
    },

});