import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  Linking,
  ImageBackground,
  Platform,
  Modal,
  ActivityIndicator
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'

class Login extends Component {
  state = {
    emailS: '',
    email: '',
    login: '',
    senha: '',
    cancelamento: 0,
    idUsuario: '',
    modalVisibleSenha: false,
  }

  static navigationOptions = {
    drawerLockMode: 'locked-closed',
  };

  componentDidMount() {

  }

  login = async () => {
    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      
      fetch(urlAPI + '/atletas/get_one.php?atleta=' + this.state.login + '&senha=' + this.state.senha, {}
      ).then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            refresh: false
          })
          
          if (!responseJson.message) {
            const RNFS = require('react-native-fs');
            const cadastro = RNFS.DocumentDirectoryPath + '/usuario.json';

            let dados = [];

            dados.push({
              idUsuario: responseJson.idAtletas,
              senha: responseJson.Senha,
              nome: responseJson.NomeCompleto,
              email: responseJson.Email,
              celular: responseJson.Celular,
              altura: responseJson.Altura,
              peso: responseJson.Peso,
              tecnico: responseJson.Tecnico,
              mediakm: responseJson.MediaKm,
              idDesk: responseJson.IdDesk
            })

           // alert(JSON.stringify(dados))

            RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
              .then((success) => {
                console.log('FILE WRITTEN!');
              })
              .catch((err) => {
                console.log(err.message);
              });

            this.props.navigation.navigate('ex')
          } else {
            alert('Usuário não encontrado')
            this.setState({
              refresh: false
            })
          }
        })
        .catch((error) => {
          alert('ocorreu um erro '+error);
          this.setState({
            refresh: false
          })
        });

    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  /* tela principal */
  render() {
    return (
      <ImageBackground source={require('../img/MMSAtletaCapa.jpg')} style={{ flex: 1, backgroundColor: '#001e46', justifyContent: 'flex-end' }}>
        <StatusBar
          barStyle="light-content"
          translucent={true}
          backgroundColor={'transparent'}
        />

        <View />

        <TextInput
          value={this.state.login}
          onChangeText={login => this.setState({ login })}
          keyboardType='email-address'
          underlineColorAndroid='transparent'
          placeholder='Login'
          placeholderTextColor='white'
          style={{
            backgroundColor: 'transparent',
            marginHorizontal: 40,
            marginBottom: 20,
            fontSize: 18,
            color: 'white',
            fontWeight: 'bold',
            borderBottomWidth: 0.7,
            borderBottomColor: 'white'
          }} />

        <TextInput
          value={this.state.senha}
          onChangeText={senha => this.setState({ senha })}
          underlineColorAndroid='transparent'
          placeholder='Senha'
          secureTextEntry={true}
          placeholderTextColor='white'
          style={{
            backgroundColor: 'transparent',
            marginHorizontal: 40,
            marginBottom: 20,
            fontSize: 18,
            color: 'white',
            fontWeight: 'bold',
            borderBottomWidth: 0.7,
            borderBottomColor: 'white'
          }} />


        <TouchableOpacity disabled={this.state.refresh} onPress={() => this.login()} style={{
          height: 40,
          width: 120,
          paddingHorizontal: 10,
          justifyContent: 'center',
          paddingVertical: 5,
          marginLeft: 40,
          marginBottom: 35,
          borderRadius: 30,
          backgroundColor: '#141b29'
        }}>
          {
            this.state.refresh ?
              <ActivityIndicator size='small' color='white' />
              :
              <Text style={{
                color: 'white',
                fontSize: 12,
                textAlign: 'center'
              }}>ENTRAR</Text>
          }
        </TouchableOpacity>

        <TouchableOpacity
          disabled={this.state.refresh}
          onPress={() => {
            const RNFS = require('react-native-fs');
            const cadastro = RNFS.DocumentDirectoryPath + '/usuario.json';

            RNFS.writeFile(cadastro, "[{}]", 'utf8')
              .then((success) => {
                console.log('FILE WRITTEN!');
              })
              .catch((err) => {
                console.log(err.message);
              });

            this.props.navigation.navigate('ex')
          }} style={{
            height: 40,
            paddingHorizontal: 10,
            justifyContent: 'center',
            marginHorizontal: 40,
            marginBottom: 10,
            borderRadius: 30,
            backgroundColor: '#141b29'
          }}>
          <Text style={{
            color: 'white',
            fontSize: 12,
            textAlign: 'center'
          }}>CONTINUAR NAVEGANDO</Text>
        </TouchableOpacity>

        <TouchableOpacity
          disabled={this.state.refresh}
          onPress={() => this.props.navigation.navigate('Cadastro')} style={{
            height: 40,
            paddingHorizontal: 10,
            justifyContent: 'center',
            marginHorizontal: 40,
            marginBottom: 35,
            borderRadius: 30,
            backgroundColor: 'white'
          }}>
          <Text style={{
            color: '#141b29',
            fontSize: 12,
            textAlign: 'center'
          }}>CADASTRE-SE</Text>
        </TouchableOpacity>
      </ImageBackground>
    )
  }
}

export default Login;

const styles = StyleSheet.create({

});