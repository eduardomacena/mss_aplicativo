import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Button,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Image,
  Alert,
  ImageBackground,
  Picker,
  Platform,
  BackHandler,
  Modal,
} from 'react-native';
import Slider from 'react-native-simple-slider';
import Icon from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Fontisto';
import IconF from 'react-native-vector-icons/FontAwesome5';
import Iconr from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconm from 'react-native-vector-icons/Entypo';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'
import {vencido} from '../utils/consultaPlano'

class LerAntes extends Component {
  state = {
    num: 0,
    value: 0,
    notaP1: null,
    items: [
      { frase: 'Responda pela escala TQR antes de iniciar o treino: Como foi a sua Recuperação?' }],
    modalVisibleSenha: false,
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('ex')
      return true;
    })

    var RNFS = require('react-native-fs');
    var path = RNFS.DocumentDirectoryPath + '/usuario.json';

    RNFS.readFile(path, 'utf8')
      .then((contents) => {
        if (!(contents == '[{}]')) {
          this.setState({
            idUsuario: String(JSON.parse(contents)[0].idUsuario),
          })

          if(vencido(String(JSON.parse(contents)[0].idUsuario))){
            alert('Seu plano expirou, renove seu plano!')
            this.props.navigation.navigate('Financeiro')
            return 
          }

          this.buscaPlano()
        } else {
          Alert.alert(
            'Confirmação',
            'Para prosseguir é necessário fazer "Login". Deseja logar agora ?',
            [
              { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
              { text: 'SIM', onPress: () => this.props.navigation.navigate('Login') },
            ]
          );

          this.props.navigation.navigate('ex')
          this.setState({ idUsuario: '0' })
        }
      })
      .catch((err) => {
        this.setState({ idUsuario: '0' })
        alert(err.message)
        console.log(err.message, err.code);
      });

  }

  avancaFrase = () => {
    this.setState({
      num: this.state.num == (this.state.items.length - 1) ? this.state.num : (this.state.num + 1)
    })
  }

  retroFrase = () => {
    this.setState({
      num: this.state.num == 0 ? 0 : (this.state.num - 1)
    })
  }

  enviarResposta = () => {
    const RNFS = require('react-native-fs');
    const cadastro = RNFS.DocumentDirectoryPath + '/lerantes.json';

    let dados = [];

    dados.push({
      TQR: this.state.value,
    })

    RNFS.writeFile(cadastro, JSON.stringify(dados), 'utf8')
      .then((success) => {
        console.log('FILE WRITTEN!');
      })
      .catch((err) => {
        console.log(err.message);
      });

    this.props.navigation.navigate('ex')

    alert('Esta informação será enviada junto com as respostas das perguntas depois do treino')

  }

  buscaPlano = async () => {
    this.setState({
      refresh: true
    })

    const isConnected = await checkInternetConnection();
    if (isConnected) {
      fetch(urlAPI + '/planos/getativos.php?atleta=' + this.state.idUsuario, {}
      ).then((response) => response.json())
        .then((responseJson) => {
          if (!responseJson.message) {

          } else {
            alert('Plano expirado, ative seu plano.')
            this.props.navigation.navigate('Financeiro')
          }
        })
        .catch((error) => {
          alert('ocorreu um erro ' + error);
          this.setState({
            refresh: false
          })
        });
    } else {
      alert('Sem conexão com a internet!')
      this.setState({
        refresh: false
      })
    }
  }

  /* tela principal */
  render() {
    return (
      <View style={{ backgroundColor: '#1b263a', flex: 1 }}>
        <StatusBar
          barStyle='light-content'
          backgroundColor='#1b263a' />

        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
          flex: 1,
          marginTop: Platform.OS == "android" ? 20 : 40,
          backgroundColor: '#1b263a'
        }}>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ marginLeft: 20, width: '33.33%' }}>
            <Iconr name="menu" color='#ff6348' style={{ padding: 5 }} size={35} />
          </TouchableOpacity>

          <Text style={{
            fontWeight: 'bold',
            fontSize: 16,
            marginTop: 12,
            color: 'white',
            marginLeft: -15
          }}>ANTES DO TREINO</Text>

          <View style={{ width: '33.33%' }} />
        </View>

        <View style={{ flex: 1, justifyContent: 'space-between', backgroundColor: 'white' }}>
          <View />
          <View
            style={{
              flexDirection: 'row',
              marginBottom: 20,
              alignItems: 'center',
              justifyContent: 'space-between',
              marginHorizontal: 20
            }}>
            <TouchableOpacity
              onPress={() => { this.retroFrase() }}
              style={{
                height: 50,
                width: 100,
                justifyContent: 'center',
                borderRadius: 40,
                backgroundColor: '#1b263a'
              }}>
              <Text style={{
                color: 'white',
                textAlign: 'center',
                fontSize: 11
              }}>VOLTAR</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => { this.enviarResposta() }}
              style={{
                height: 50,
                width: 100,
                justifyContent: 'center',
                borderRadius: 40,
                backgroundColor: '#2ecc71'
              }}>
              <Text style={{
                color: 'white',
                textAlign: 'center',
                fontSize: 11
              }}>ENVIAR</Text>
            </TouchableOpacity>
          </View>

        </View>

        <View style={{
          position: 'absolute',
          height: '60%',
          marginHorizontal: '10%',
          alignSelf: 'center',
          justifyContent: 'center',
          borderRadius: 20,
          marginTop: '30%',
          alignItems: 'center',
          backgroundColor: 'white',
          elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
        }}>
          <Text style={{
            fontSize: 20,
            fontWeight: 'bold',
            textAlign: 'center',
            marginHorizontal: 40
          }}>
            {this.state.items[this.state.num].frase}
          </Text>

          <Text style={{
            fontSize: 20,
            fontWeight: 'bold',
            textAlign: 'center',
            marginTop: 40,
            color: this.state.value == 0 ? '#c0392b' :
              this.state.value == 1 ? '#e74c3c' :
                this.state.value == 2 ? '#ff7979' :
                  this.state.value == 3 ? '#ff7979' :
                    this.state.value == 4 ? '#ffbe76' :
                      this.state.value == 5 ? '#ffbe76' :
                        this.state.value == 6 ? '#7bed9f' :
                          this.state.value == 7 ? '#7bed9f' :
                            this.state.value == 8 ? '#2ed573' :
                              this.state.value == 9 ? '#2ed573' :
                                this.state.value == 10 ? '#27ae60' : null
            ,
            marginHorizontal: 40
          }}>
            {this.state.value == 0 ? 'Quase Nada' :
              this.state.value == 1 ? 'Muito Pouca' :
                this.state.value == 2 ? 'Pouca' :
                  this.state.value == 3 ? 'Pouca' :
                    this.state.value == 4 ? 'Razoável' :
                      this.state.value == 5 ? 'Razoável' :
                        this.state.value == 6 ? 'Boa' :
                          this.state.value == 7 ? 'Boa' :
                            this.state.value == 8 ? 'Muito Boa' :
                              this.state.value == 9 ? 'Muito Boa' :
                                this.state.value == 10 ? 'Excelente' : null
            }
          </Text>

          <View style={{
            marginTop: 40,
            alignSelf: 'center',
            justifyContent: 'space-between',
          }}>

            <Slider
              value={this.state.value}
              minimumValue={0}
              maximumValue={10}
              sliderHeight={20}
              thumbTintColor={'#1b263a'}
              minimumTrackTintColor={this.state.value == 0 ? '#c0392b' :
                this.state.value == 1 ? '#e74c3c' :
                  this.state.value == 2 ? '#ff7979' :
                    this.state.value == 3 ? '#ff7979' :
                      this.state.value == 4 ? '#ffbe76' :
                        this.state.value == 5 ? '#ffbe76' :
                          this.state.value == 6 ? '#7bed9f' :
                            this.state.value == 7 ? '#7bed9f' :
                              this.state.value == 8 ? '#2ed573' :
                                this.state.value == 9 ? '#2ed573' :
                                  this.state.value == 10 ? '#27ae60' : null}
              thumbButtonSize={30}
              sliderWidth={200}
              sliderBorderRadius={30}
              onValueChange={value => {
                this.setState({ value: parseInt(value) })
              }}
              disabledHoverEffect={false}
            />
          </View>

        </View>
      </View>
    )
  }
}

export default LerAntes;