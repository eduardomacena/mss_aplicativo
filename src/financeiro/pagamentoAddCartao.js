import React, { Component } from 'react'
import {
    Text,
    TextInput,
    StyleSheet,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    Alert,
    Platform,
    BackHandler,
    ActivityIndicator,
    View
} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';
import IconF from 'react-native-vector-icons/FontAwesome';
import { checkInternetConnection } from 'react-native-offline';
import { urlAPI } from '../utils/API'
import { tgdeveloper } from '../utils/validaBandeira'

export default class pagamentoAddCartao extends Component {

    state = {
        vencimento: '',
        cvc: '',
        cartao: '',
        plano: [],
        refresh: false
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate('Financa')
            return true;
        })

        var RNFS = require('react-native-fs');
        var path = RNFS.DocumentDirectoryPath + '/ativaPlano.json';

        RNFS.readFile(path, 'utf8')
            .then((contents) => {
                if (!(contents == '[{}]') & !(contents == '')) {
                    this.setState({
                        plano: JSON.parse(contents),
                    })
                } else {
                    alert('Selecione um plano para ativar.');

                    this.props.navigation.navigate('Planos')
                }
            })
            .catch((err) => {
                alert(err.message)
                console.log(err.message, err.code);
            });

    }

    ativaPlano = async () => {
        this.setState({
            refresh: true
        })

        const isConnected = await checkInternetConnection();
        if (isConnected) {
            fetch(urlAPI + '/planos/ativa.php?atleta=' + this.state.plano[0].idAtleta + '&plano=' + this.state.plano[0].idPlano, {}
            ).then((response) => response.json())
                .then((responseJson) => {

                    if (responseJson.message == 'sucesso') {
                        this.setState({
                            refresh: false
                        })
                        // alert('Plano ativo com sucesso')
                        this.props.navigation.navigate('ex')
                    } else {
                        this.setState({
                            refresh: false
                        })
                        alert('Falhou, seu plano foi pago mais não registrado entre em contato com o suporte!')
                        this.props.navigation.navigate('ex')
                    }
                })
                .catch((error) => {
                    alert('ocorreu um erro ' + error);
                    this.setState({
                        refresh: false
                    })
                });
        } else {
            alert('Sem conexão com a internet!')
            this.setState({
                refresh: false
            })
        }
    }

    pagamento = async () => {
        this.setState({
            refresh: true
        })

        let dados = {
            "MerchantOrderId": "1",
            "Customer": {
                "Name": "Cliente ID - " + this.state.plano[0].idAtleta
            },
            "Payment": {
                "Type": "CreditCard",
                "Amount": parseInt(parseFloat(this.state.plano[0].valor) * 100),
                "Installments": (parseInt(this.state.plano[0].qtdParcela) * 1),
                "SoftDescriptor": "MSSYSTEM",
                "CreditCard": {
                    "CardNumber": String(this.state.cartao),
                    "Holder": "",
                    "ExpirationDate": String(this.state.vencimento),
                    "SecurityCode": String(this.state.cvc),
                    "Brand": tgdeveloper.getCardFlag(String(this.state.cartao)),
                },
                "IsCryptoCurrencyNegotiation": false
            }
        }

        const isConnected = await checkInternetConnection();
        if (isConnected) {
            fetch('https://api.cieloecommerce.cielo.com.br/1/sales/', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'MerchantId': 'a6af1b01-5007-4bb2-9080-9d45ec33c846',
                    'MerchantKey': 'leceaxQHy5HG36rSQ4pfo2pte9OmH2QW4NIYOEzR'
                },
                body: JSON.stringify(dados)
            }
            ).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.Payment.ReturnMessage == 'Transacao autorizada') {
                        this.setState({
                            refresh: false
                        })
                        this.ativaPlano()
                        alert('Plano ativo com sucesso')
                    } else {
                        this.setState({
                            refresh: false
                        })
                        alert('Falhou tente mais tarde!')
                        this.props.navigation.navigate('ex')
                    }
                })
                .catch((error) => {
                    alert('ocorreu um erro ' + error);
                    this.setState({
                        refresh: false
                    })
                });
        } else {
            alert('Sem conexão com a internet!')
            this.setState({
                refresh: false
            })
        }
    }

    parseDate = (str) => {
        let text = str.replace(/[^0-9]/g, '');
        let m = ''

        for (let index = 0; index < String(text).length;) {

            if (index == 2) {
                m = m + '/' + text[index] 
            } else {
                m = m + text[index]
            }

            index++
        }

        return m;
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: "white"
            }}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor='#141b29'
                />

                <View
                    style={{
                        height: Platform.OS == 'android' ? 55 : 85,
                        justifyContent: "flex-end",
                        backgroundColor: '#141b29'
                    }}>
                    <View
                        style={{
                            flexDirection: "row",
                            alignItems: "flex-end",
                            justifyContent: "space-between"
                        }}>
                        <View style={{
                            flexDirection: "row",
                            alignItems: "flex-end",
                        }}>
                            <TouchableOpacity
                                style={{
                                    marginLeft: 20,
                                    marginBottom: 5,
                                }}
                                onPress={() => { this.props.navigation.navigate('ex') }}>

                                <Icon name={"arrowleft"} color="#ff6348" size={30} />
                            </TouchableOpacity>

                            <Text
                                style={{
                                    textAlign: 'center',
                                    marginBottom: Platform.OS == 'android' ? 10 : 14,
                                    marginLeft: 25,
                                    fontSize: 17,
                                    fontWeight: 'bold',
                                    color: 'white'
                                }}>Adicionar cartão</Text>
                        </View>

                        <TouchableOpacity
                            style={{
                                marginRight: 20,
                                marginBottom: 5,
                            }}
                            onPress={() => {
                                if (!(this.state.vencimento == '') |
                                    !(this.state.cartao == '') |
                                    !(this.state.cvc == '')) {
                                    Alert.alert(
                                        'Confirmação',
                                        'Certeza que deseja assinar o plano "' + this.state.plano[0].tipo + '" ?',
                                        [
                                            { text: 'NÃO', onPress: () => console.warn('NO Pressed'), style: 'cancel' },
                                            { text: 'SIM', onPress: () => this.pagamento() },
                                        ]
                                    );
                                } else {
                                    alert('Preencha todos os campos, corretamente!')
                                }
                            }}>

                            <Icon name={"check"} color="#ff6348" size={30} />
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={{
                    flexDirection: "row",
                    paddingHorizontal: 10,
                    height: 40,
                    alignItems: "center",
                    borderBottomWidth: 0.3,
                }}>
                    <TextInput
                        placeholder="Número do Cartão"
                        value={this.state.cartao}
                        onChangeText={(cartao) => { this.setState({ cartao }) }}
                        keyboardType="numeric"
                        maxLength={16}
                        style={{
                            flex: 1
                        }}
                    />

                    <Icon name={"creditcard"} color='#141b29' size={30} />
                </View>

                <View style={{
                    flexDirection: "row",
                    paddingHorizontal: 10,
                    height: 40,
                    alignItems: "center",
                    borderBottomWidth: 0.3,

                }}>
                    <View style={{
                        flex: 1,
                    }}>
                        <TextInput
                            maxLength={7}
                            value={this.state.vencimento}
                            onChangeText={(vencimento) => { this.setState({ vencimento: this.parseDate(vencimento) }) }}
                            style={{ flex: 1, }}
                            placeholder="08/2025"
                            keyboardType="numeric"
                        />
                    </View>
                    <View style={{
                        flex: 1,
                        paddingLeft: 5,
                        borderLeftWidth: 0.3
                    }}>
                        <TextInput
                            maxLength={3}
                            value={this.state.cvc}
                            onChangeText={(cvc) => { this.setState({ cvc }) }}
                            style={{ flex: 1, }}
                            placeholder="CVC"
                            keyboardType="numeric"
                        />
                    </View>
                </View>

                <View style={{
                    flexDirection: "row",
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    alignItems: "center",
                    borderBottomWidth: 0.3,

                }}>
                    <Text style={{
                        marginLeft: 10,
                    }}>Suas informações de pagamentos não são armazenadas por segurança <Text style={{ color: "red" }}>*</Text></Text>
                </View>

                {
                    this.state.refresh ? <ActivityIndicator
                        size="large"
                        color="white"
                        style={{
                            position: "absolute",
                            height: '100%',
                            width: '100%',
                            backgroundColor: 'rgba(0,0,0,0.6)'
                        }}
                    /> : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({})
